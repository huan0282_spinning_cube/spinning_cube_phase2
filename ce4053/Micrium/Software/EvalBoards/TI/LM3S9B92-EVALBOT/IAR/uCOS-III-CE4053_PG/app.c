/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : FUZZI
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>
#include <stdio.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"

#include "ext/SD_Card.h"

/*
*********************************************************************************************************
*                                             Fibonacci function and node declaration,local var
*********************************************************************************************************
*/
/*
 * Node Declaration
 */
#define MAX 10000
#define APP_TICK_WHEEL_SIZE 12

struct node
{
    CPU_INT32U  n; //n is the period value
    CPU_INT32U  degree;
    CPU_INT32U  cur_ctr;
    CPU_INT32U  nxt_match_ctr; //n is the next tick counter match value
    CPU_INT32U  remain_ctr; // use for fibo sort
    CPU_INT32U  exe; //indicate if executed in the current counter or not
    CPU_INT32U  task_num; //denote task no
    struct node* parent;
    struct node* child;
    struct node* left;
    struct node* right;
};

struct rdy_q_node {
    struct rdy_q_node * parent;
    struct rdy_q_node * left;
    struct rdy_q_node * right;
    CPU_INT32U value;
    CPU_INT32U index;
    CPU_INT32U n; //record period
};

struct  app_tick_spoke {
    struct node          *FirstPtr;                          /* Pointer to list of tasks node in tick spoke                 */
    int                   NbrEntries;                        /* Current number of entries in the tick spoke            */
    int                   NbrEntriesMax;                     /* Peak number of entries in the tick spoke               */
};

//Tick wheel implementation
static struct app_tick_spoke APPCfg_TickWheel[APP_TICK_WHEEL_SIZE];
int    NON_ZERO_SPOKE[APP_TICK_WHEEL_SIZE]; //Store the non-zero-entry spoke number in this array.
int    NON_ZERO_SPOKE_NUM; 
static struct  app_tick_spoke *p_spoke1;
static struct  app_tick_spoke *p_spoke2;
static struct node *node_task_ptr;
static struct node *node_task_ptr_l;
static struct node *node_task_ptr_r;
static int          NbrEntries_tmp;

//Time measurement
CPU_TS   StartTime = 0;  
CPU_TS   StartTime2 = 0; 
CPU_TS   StartTime3 = 0; 
CPU_TS   OverheadValue = 0;  

//For AVL physical node creation
static struct rdy_q_node * root;
static struct rdy_q_node * nil;
static CPU_INT32U h[MAX]; // keep values for the heights of the sub trees
static CPU_INT32U tree_size;

struct rdy_q_node avl_null_node;
struct rdy_q_node t1_rdy;
struct rdy_q_node t2_rdy; 
struct rdy_q_node t3_rdy; 
struct rdy_q_node t4_rdy;
struct rdy_q_node t5_rdy; 
static  struct rdy_q_node *min_rdy_ptr_imm;


struct rdy_q_node* init_tree();
void left_rotate(struct rdy_q_node * x);
void right_rotate(struct rdy_q_node * x);
CPU_INT32U height(struct rdy_q_node * x);
void balance(struct rdy_q_node * x);
void add_to_rdy_q_node(struct rdy_q_node* tx_rdy,CPU_INT32U a, CPU_INT32U n);
//void print(struct rdy_q_node * el, CPU_INT32U i);
struct rdy_q_node* search(CPU_INT32U x);
void transplant(struct rdy_q_node * node1,struct  rdy_q_node * node2);
struct rdy_q_node* minimum(struct rdy_q_node * node);
void delete_balance(struct rdy_q_node * x);
//void delete_from_rdy_q_node(CPU_INT32U val);
void delete_from_rdy_q_node(struct rdy_q_node * x);


struct node* Insert(struct node* H, struct node* x);
struct node* Extract_Min(struct node* x);
void Fibo_Sort(struct node* x);
//int Consolidate(struct node* H1);

int      nH = 0;
struct node* H;  //a node pointer that always point to the minimum
struct node* prev_exe_t_ptr;  //a node pointer that point to the previous executed task node
struct node  prev_exe_t;  //a copy of previous executed task node
struct node t1_move;
struct node t2_rightLed; 
struct node t3_leftLed; 

CPU_INT16U   leftLEDBlink_executed = 0; 
CPU_INT16U   rightLEDBlink_executed = 0;   
CPU_INT16U   move_executed = 0;   
CPU_INT32U   prev_degree = 0;   //to decide weather to repeat and resort from the left most position again
CPU_INT32U   cur_degree = 0;   //to decide weather to repeat and resort from the left most position again

//Benchmarking
CPU_TS app1_start_mutex2_get_time;
CPU_TS app1_start_mutex2_rel_time;
CPU_TS app1_start_mutex3_get_time;
CPU_TS app1_start_mutex3_rel_time;
CPU_TS app1_mutex2_get_overhead;
CPU_TS app1_mutex2_rel_overhead;
CPU_TS app1_mutex3_get_overhead;
CPU_TS app1_mutex3_rel_overhead;
CPU_TS app1_exe_time;
CPU_TS app1_exe_time_tmp;

CPU_TS app2_start_mutex2_get_time;
CPU_TS app2_start_mutex2_rel_time;
CPU_TS app2_start_mutex3_get_time;
CPU_TS app2_start_mutex3_rel_time;
CPU_TS app2_mutex2_get_overhead;
CPU_TS app2_mutex2_rel_overhead;
CPU_TS app2_mutex3_get_overhead;
CPU_TS app2_mutex3_rel_overhead;
CPU_TS app2_exe_time;
CPU_TS app2_exe_time_tmp;

CPU_TS app3_start_mutex1_get_time;
CPU_TS app3_start_mutex1_rel_time;
CPU_TS app3_mutex1_get_overhead;
CPU_TS app3_mutex1_rel_overhead;
CPU_TS app3_exe_time;
CPU_TS app3_exe_time_tmp;

CPU_TS app1_time1;
CPU_TS app1_time2;
CPU_TS app2_time1;
CPU_TS app2_time2;
CPU_TS app3_time1;
CPU_TS app3_time2;

/*
*********************************************************************************************************
*                                             LOCAL DEFINES
*********************************************************************************************************
*/

#define ONESECONDTICK             7000000

#define TASK1PERIOD                   5
#define TASK2PERIOD                   5
#define TASK3PERIOD                   10

#define WORKLOAD1                    2
#define WORKLOAD2                    2
#define WORKLOAD3                    1

#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz)

#define MICROSD_EN                    0
#define TIMER_EN                      0

/*
*********************************************************************************************************
*                                            LOCAL VARIABLES
*********************************************************************************************************
*/

static  OS_TCB       AppTaskStartTCB;
static  CPU_STK      AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  OS_TCB       AppTaskOneTCB;
static  CPU_STK      AppTaskOneStk[APP_TASK_ONE_STK_SIZE];

static  OS_TCB       AppTaskTwoTCB;
static  CPU_STK      AppTaskTwoStk[APP_TASK_TWO_STK_SIZE];

static  OS_TCB       AppTaskThreeTCB;
static  CPU_STK      AppTaskThreeStk[APP_TASK_THREE_STK_SIZE];

static  CPU_STK      OS_RecTaskStk[OS_RecTask_STK_SIZE];

OS_Q            DummyQ;
OS_MUTEX        MutexOne, MutexTwo, MutexThree;

CPU_INT32U      iCnt = 0;
CPU_INT08U      Left_tgt;
CPU_INT08U      Right_tgt;

CPU_INT32U      iToken  = 0;
CPU_INT32U      iCounter= 1;
CPU_INT32U      iMove   = 10;
OS_FLAGS        flag_0 = 0;
CPU_CHAR        g_cCmdBuf[30] = {'n','e','w',' ','l','o','g','.','t','x','t','\0'};


/*
*********************************************************************************************************
*                                            USER ADDED LOCAL VARIABLES
*********************************************************************************************************
*/

OS_SEM SEM_TASK1;
OS_SEM SEM_TASK2;
OS_SEM SEM_TASK3;
OS_SEM SEM_TASK123;
#define  TASK1_OK   0x01
#define  TASK2_OK   0x02
#define  TASK3_OK   0x04
OS_FLAG_GRP  StatusFlags;

CPU_INT32U TASK1_FUNC;
CPU_INT32U TASK2_FUNC;
CPU_INT32U TASK3_FUNC;

/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

        void        IntWheelSensor                    ();
static  void        AppRobotMotorDriveSensorEnable    ();

        void        RoboTurn                          (tSide dir, CPU_INT16U seg, CPU_INT16U speed);

static  void        AppTaskStart                 (void  *p_arg);
static  void        AppTaskOne                   (void  *p_arg);
static  void        AppTaskTwo                   (void  *p_arg);
static  void        AppTaskThree                 (void  *p_arg);

static  void        OS_RecTaskCreate             (void  *p_arg);
void  APP_TickListInit (void);
void  APP_TickListRemove (int spoke);
void  APP_TickListInsert (struct node *task_node,int app_sec);

#if(TIMER_EN == 1)
extern void TimerReset(void);
extern unsigned long TimerTick(void);
unsigned long iTick1, iTick2, iTick3,iTick4;
#endif

/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/

int  main (void)
{
    OS_ERR  err;

    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,           /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) (CPU_INT32U) 0, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSFlagCreate(&StatusFlags,(CPU_CHAR *)"StatusFlags",(OS_FLAGS)0,&err);   
    OSSemCreate(&SEM_TASK123,"Semo_T3",0,&err);
    
    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void  *p_arg)
{
    CPU_INT32U  clk_freq;
    CPU_INT32U  ulPHYMR0;
    CPU_INT32U  cnts;
    OS_ERR      err;

   (void)&p_arg;

    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ETH);                  /* Enable and Reset the Ethernet Controller.            */
    SysCtlPeripheralReset(SYSCTL_PERIPH_ETH);
    ulPHYMR0 = EthernetPHYRead(ETH_BASE, PHY_MR0);              /* Power Down PHY                                       */
    EthernetPHYWrite(ETH_BASE, PHY_MR0, ulPHYMR0 | PHY_MR0_PWRDN);
    SysCtlPeripheralDeepSleepDisable(SYSCTL_PERIPH_ETH);
    clk_freq = BSP_CPUClkFreq();                                /* Determine SysTick reference freq.                    */
    cnts     = clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* Determine nbr SysTick increments                     */
    OS_CPU_SysTickInit(cnts);                                   /* Init uC/OS periodic time src (SysTick).              */
    CPU_TS_TmrFreqSet(clk_freq);
    
    #if(MICROSD_EN == 1)
    /* Mount the file system, using logical disk 0 */
    f_mount(0, &g_sFatFs);
    /* Create a new log.txt file */
    CmdLineProcess(g_cCmdBuf);
    #endif

    /* Enable Wheel ISR Interrupt */
    AppRobotMotorDriveSensorEnable();
    
    /* Create Dummy Queue */
    OSQCreate((OS_Q *)&DummyQ, (CPU_CHAR *)"Dummy Queue", (OS_MSG_QTY)5, (OS_ERR *)&err);
    
    /* Create Mutexes */
    OSMutexCreate((OS_MUTEX *)&MutexOne, (CPU_CHAR *)"a", (OS_ERR *)&err);
    OSMutexCreate((OS_MUTEX *)&MutexTwo, (CPU_CHAR *)"b", (OS_ERR *)&err);
    OSMutexCreate((OS_MUTEX *)&MutexThree, (CPU_CHAR *)"c", (OS_ERR *)&err);
    
    //Create a recursive task , using OSTaskCreate, using the same stack size as TASK 1 size
    OSTaskCreate((OS_TCB     *)&OS_RecTaskTCB, (CPU_CHAR   *)"OS_RecTaskCreate", (OS_TASK_PTR ) OS_RecTaskCreate, (void       *) 0, (OS_PRIO     ) OS_RecTask_PRIO, (CPU_STK    *)&OS_RecTaskStk[0], (CPU_STK_SIZE) OS_RecTask_STK_SIZE / 10u, (CPU_STK_SIZE) OS_RecTask_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    
    /* Start!! */
    BSP_DisplayStringDraw("Go",0u, 1u);
    
    /* Delete this task */
    OSTaskDel((OS_TCB *)0, &err);
}

static void OS_RecTaskCreate (void *p_arg)
{ 
   OS_ERR      err;
  (void)&p_arg;
  CPU_INT32U      APP_1SEC_CTR = 0;
  CPU_INT32U      APP_1SEC_CTR_TMP = 0;
  CPU_TS  ts;
  CPU_TS  diff;
  CPU_INT16U   leftLEDBlink_Exe_PRIO;                             
  CPU_INT16U   rightLEDBlink_Exe_PRIO;                             
  CPU_INT16U   move_Exe_PRIO; 
  CPU_INT32U t1_deadline;
  CPU_INT32U t2_deadline;
  CPU_INT32U t3_deadline; 
  CPU_INT32U min;
  CPU_INT32U prev_prior;
  CPU_INT32U init_tree_v;
  CPU_INT32U avl_exe_right = 0;
  CPU_INT16U task_num_exe = 0;  
  OS_FLAGS value;
  int nxt_match_ctr_found;

  
  CPU_INT16U t1_move_exe_tmp;
  CPU_INT16U t2_rightLed_exe_tmp;
  CPU_INT16U t3_leftLed_exe_tmp;
  
  //For AVL
  struct rdy_q_node root_org; //always record the AVL root node physically befor OSCREATE TASK 
  struct rdy_q_node *min_rdy_q_node;
  
  CPU_INT32U APP_TICK_SPOKE;  
  
  min_rdy_q_node = NULL;  
  //End of AVL variable
  
  //Init H to a NULL value first
  H = NULL;
  
  APP_TickListInit(); //Initialize the tick wheel
  
  //Create tast instances and insert,at initial value

  t3_leftLed.n = 10u;
  t3_leftLed.task_num = 3u;
  t3_leftLed.cur_ctr = 0u; 
  t3_leftLed.nxt_match_ctr = t3_leftLed.n + 0u; // n is period
  t3_leftLed.remain_ctr = t3_leftLed.nxt_match_ctr - t3_leftLed.cur_ctr; // cal remain count
  H = Insert(H,&t3_leftLed);  
  APP_TickListInsert(&t3_leftLed,0);

  t1_move.n = 5u;
  t1_move.task_num = 1u;
  t1_move.cur_ctr = 0u; 
  t1_move.nxt_match_ctr = t1_move.n + 0u; // n is period
  t1_move.remain_ctr = t1_move.nxt_match_ctr - t1_move.cur_ctr; // cal remain count
  H = Insert(H,&t1_move);
  APP_TickListInsert(&t1_move,0);
  
  t2_rightLed.n = 5u;
  t2_rightLed.task_num = 2u;
  t2_rightLed.cur_ctr = 0u; 
  t2_rightLed.nxt_match_ctr = t2_rightLed.n + 0u; // n is period
  t2_rightLed.remain_ctr = t2_rightLed.nxt_match_ctr - t2_rightLed.cur_ctr; // cal remain count
  H = Insert(H,&t2_rightLed);
  APP_TickListInsert(&t2_rightLed,0);
  
  //Semophore create for synchronous release

  
    OSTaskCreate((OS_TCB     *)&AppTaskOneTCB, (CPU_CHAR   *)"1", (OS_TASK_PTR ) AppTaskOne, (void       *) 0, (OS_PRIO     ) APP_TASK_ONE_PRIO, (CPU_STK    *)&AppTaskOneStk[0], (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *)(CPU_INT32U) 1, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    OSTaskCreate((OS_TCB     *)&AppTaskTwoTCB, (CPU_CHAR   *)"2", (OS_TASK_PTR ) AppTaskTwo, (void       *) 0, (OS_PRIO     ) APP_TASK_TWO_PRIO, (CPU_STK    *)&AppTaskTwoStk[0], (CPU_STK_SIZE) APP_TASK_TWO_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_TWO_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    OSTaskCreate((OS_TCB     *)&AppTaskThreeTCB, (CPU_CHAR   *)"3", (OS_TASK_PTR ) AppTaskThree, (void       *) 0, (OS_PRIO     ) APP_TASK_THREE_PRIO, (CPU_STK    *)&AppTaskThreeStk[0], (CPU_STK_SIZE) APP_TASK_THREE_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_THREE_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 3, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    //Semophore creation for synchronous release
   
    value = 0;
    while (value == 0){
     if (StatusFlags.Flags == 0x7) {value = 1;StatusFlags.Flags = 0x0;}
    }
    OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL,&err);
    
    init_tree_v = 0;
    //Start all the tast after time T=0, which is T>0
    while(1)
    {
      
        (void)OSTaskSemPend(0,
          OS_OPT_PEND_BLOCKING,
          &ts,
          &err);
         
         if (BENCHMARK_CODE > 0) {        
            StartTime = OS_TS_GET();
            StartTime2 = OS_TS_GET();
         }

          //Init the AVL scheduler tree first if the init_tree is 0;
         if (init_tree_v == 0) {
           nil = init_tree();
           root = init_tree();
           init_tree_v = 1;
         }
         
          APP_1SEC_CTR_TMP = APP_1SEC_CTR_TMP + 1;
          APP_1SEC_CTR = APP_1SEC_CTR + 1;
 
         //Search for the Tick spoke base on APP_TICK_SPOKE
         //Search from the FIBO root list to check all the node task that got the same tick spoke, starting from the H node                   
         //Implement fibonacci heap search for the match tick counter task and create and execute and delete the task of the match counter
               
          //Tick Wheel implementation, compute for the spoke;
          APP_TICK_SPOKE = APP_1SEC_CTR % APP_TICK_WHEEL_SIZE;          
          
          //Search and update only when the dedicated spoke is non-zero entries
          p_spoke1 = &APPCfg_TickWheel[APP_TICK_SPOKE];
          NbrEntries_tmp = p_spoke1->NbrEntries; //temporary store the value
          if (p_spoke1->NbrEntries != 0) { //If the NbrEntries is non-zero, means there are task in this spoke and ready to release
            
            if(APP_TICK_SPOKE != 10) { //no need to reset
                 //Assign the time out task node pointer from the  p_spoke1->FirstPtr to node_task_ptr
              if (p_spoke1->NbrEntries == 1) { //if NbrEntries == 1, only 1 task node in this spoke
                    node_task_ptr = p_spoke1->FirstPtr;
              } else if (p_spoke1->NbrEntries == 2){//if NbrEntries > 1, 2 task node in this spoke
                    node_task_ptr = p_spoke1->FirstPtr;
                    node_task_ptr_l = node_task_ptr->left;
                    nxt_match_ctr_found = 0;
                    while (node_task_ptr_l != NULL && nxt_match_ctr_found == 0) { //keep searching on the root list untill another nxt_time match is found
                      if (node_task_ptr_l->nxt_match_ctr == APP_1SEC_CTR) {
                        nxt_match_ctr_found = 1;
                      } else {
                      node_task_ptr_l = node_task_ptr_l->left;
                      }
                    }
                    
                    //Search to the right again if the nxt_match_ctr_found is still 0
                    if (nxt_match_ctr_found = 0){
                    node_task_ptr_l = node_task_ptr->right;
                    while (node_task_ptr_l != NULL && nxt_match_ctr_found == 0) { //keep searching on the root list untill another nxt_time match is found
                      if (node_task_ptr_l->nxt_match_ctr == APP_1SEC_CTR) {
                        nxt_match_ctr_found = 1;
                      } else {
                        node_task_ptr_l = node_task_ptr_l->right;
                      }
                    }        
                    }
                    
              }

                 node_task_ptr->exe  = 1;
                 task_num_exe = task_num_exe + 1;
                 
                 //Delete this node and Perform Sorting of the remaining nodes upon deletion of this node,
                 Fibo_Sort(node_task_ptr);
                 
                // update for next release of the task intance
                node_task_ptr->cur_ctr = APP_1SEC_CTR;//update for next occurrance
                node_task_ptr->nxt_match_ctr = node_task_ptr->n + node_task_ptr->cur_ctr; // n is period
                node_task_ptr->remain_ctr = node_task_ptr->nxt_match_ctr - node_task_ptr->cur_ctr; // cal remain count   
                
                //insert back
                H = Insert(H,node_task_ptr);
                
                //remove the entire spoke from the tick wheel array
                APP_TickListRemove(APP_TICK_SPOKE);
                
               //calculate deadline
                if (node_task_ptr->task_num == 1){
                    t1_deadline = t1_move.cur_ctr + t1_move.n;
                    add_to_rdy_q_node(&t1_rdy,t1_deadline,t1_move.n);
                    
                    if (t1_deadline <= root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t1_rdy;
                            
                    APP_TickListInsert(&t1_move,APP_1SEC_CTR); //insert back to tick list for next release
                }
                
                if (node_task_ptr->task_num == 2){
                    t2_deadline = t2_rightLed.cur_ctr + t2_rightLed.n;
                    add_to_rdy_q_node(&t2_rdy,t2_deadline,t2_rightLed.n);  
                    
                    if (t2_deadline < root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t2_rdy;
                    
                    APP_TickListInsert(&t2_rightLed,APP_1SEC_CTR); //insert back to tick list for next release
                }
                
                if (node_task_ptr->task_num == 3){
                    t3_deadline = t3_leftLed.cur_ctr + t3_leftLed.n;
                    add_to_rdy_q_node(&t3_rdy,t3_deadline,t3_leftLed.n); 
                    
                    if (t3_deadline < root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t3_rdy;    
                    
                    APP_TickListInsert(&t3_leftLed,APP_1SEC_CTR); //insert back to tick list for next release
                }
   
                if (NbrEntries_tmp == 2){ //if the NbrEntries == 2, have to update the other task node as well
                 node_task_ptr_l->exe  = 1;
                 task_num_exe = task_num_exe + 1;
                 
                 //Delete this node and Perform Sorting of the remaining nodes upon deletion of this node,
                 Fibo_Sort(node_task_ptr_l);
                 
                // update for next release of the task intance
                node_task_ptr_l->cur_ctr = APP_1SEC_CTR;//update for next occurrance
                node_task_ptr_l->nxt_match_ctr = node_task_ptr_l->n + node_task_ptr_l->cur_ctr; // n is period
                node_task_ptr_l->remain_ctr = node_task_ptr_l->nxt_match_ctr - node_task_ptr_l->cur_ctr; // cal remain count   
                
                //insert back
                H = Insert(H,node_task_ptr_l);
                
               //calculate deadline
                if (node_task_ptr_l->task_num == 1){
                    t1_deadline = t1_move.cur_ctr + t1_move.n;
                    add_to_rdy_q_node(&t1_rdy,t1_deadline,t1_move.n);
                    
                    if (t1_deadline <= root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t1_rdy;    
                    
                    APP_TickListInsert(&t1_move,APP_1SEC_CTR); //insert back to tick list for next release
                }
                
                if (node_task_ptr_l->task_num == 2){
                    t2_deadline = t2_rightLed.cur_ctr + t2_rightLed.n;
                    add_to_rdy_q_node(&t2_rdy,t2_deadline,t2_rightLed.n);
                    
                    if (t2_deadline < root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t2_rdy;           
                    
                    APP_TickListInsert(&t2_rightLed,APP_1SEC_CTR); //insert back to tick list for next release
                }
                
                if (node_task_ptr_l->task_num == 3){
                    t3_deadline = t3_leftLed.cur_ctr + t3_leftLed.n;
                    add_to_rdy_q_node(&t3_rdy,t3_deadline,t3_leftLed.n);  
                    
                    if (t3_deadline < root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t3_rdy;    
                    
                    APP_TickListInsert(&t3_leftLed,APP_1SEC_CTR); //insert back to tick list for next release
                }                           
                  
                }
                

            } else if (APP_TICK_SPOKE == 10){//need to reset all the task node counter and the APP_1SEC_CTR and reinsert back
                                              //All 3 task need to release

              APP_1SEC_CTR = 0;
              NON_ZERO_SPOKE[APP_TICK_SPOKE] = 0;
              NON_ZERO_SPOKE_NUM = 0; 
              APP_TickListRemove(APP_TICK_SPOKE);
              
              t1_move.exe = 1;
              task_num_exe = task_num_exe + 1;
              //Delete this node and Perform Sorting of the remaining nodes upon deletion of this node,
              Fibo_Sort(&t1_move);  
              
              // update for next release of the T1 task intance
              t1_move.cur_ctr = APP_1SEC_CTR;//update for next occurrance
              t1_move.nxt_match_ctr = t1_move.n + t1_move.cur_ctr; // n is period
              t1_move.remain_ctr = t1_move.nxt_match_ctr - t1_move.cur_ctr; // cal remain count
              APP_TickListInsert(&t1_move,0);
                
              //insert back
              H = Insert(H,&t1_move);
                
              //calculate deadline
              t1_deadline = t1_move.cur_ctr + t1_move.n;
              add_to_rdy_q_node(&t1_rdy,t1_deadline,t1_move.n);  
              
              if (t1_deadline <= root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t1_rdy;    
                    
                task_num_exe = task_num_exe + 1;
                t2_rightLed.exe = 1;
                //Delete this node and Perform Sorting upon deletion of this node
                Fibo_Sort(&t2_rightLed); 
                
                //H=Extract_Min(); //update the min in H pointer, already did in Fibo_sort      
                t2_rightLed.cur_ctr = APP_1SEC_CTR;//update for next occurrance
                t2_rightLed.nxt_match_ctr = t2_rightLed.n + t2_rightLed.cur_ctr; // n is period
                t2_rightLed.remain_ctr = t2_rightLed.nxt_match_ctr - t2_rightLed.cur_ctr; // cal remain count    
                APP_TickListInsert(&t2_rightLed,0); 
                
                //insert back
                H = Insert(H,&t2_rightLed); 
                
                //calculate deadline
                t2_deadline = t2_rightLed.cur_ctr + t2_rightLed.n;
                add_to_rdy_q_node(&t2_rdy,t2_deadline,t2_rightLed.n);   
                
                if (t2_deadline < root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t2_rdy;  
              
              task_num_exe = task_num_exe + 1;
              t3_leftLed.exe = 1;
                //Delete this node and Perform Sorting upon deletion of this node
                Fibo_Sort(&t3_leftLed);
                               
                t3_leftLed.cur_ctr = APP_1SEC_CTR;//update for next occurrance
                t3_leftLed.nxt_match_ctr = t3_leftLed.n + t3_leftLed.cur_ctr; // n is period
                t3_leftLed.remain_ctr = t3_leftLed.nxt_match_ctr - t3_leftLed.cur_ctr; // cal remain count 
                APP_TickListInsert(&t3_leftLed,0);
                //insert back
                H = Insert(H,&t3_leftLed);       
                
              //calculate deadline
              t3_deadline = t3_leftLed.cur_ctr + t3_leftLed.n;
              add_to_rdy_q_node(&t3_rdy,t3_deadline,t3_leftLed.n);     
              if (t3_deadline < root->value) //assign the minimum deadline pointer on the spot
                          min_rdy_ptr_imm = &t3_rdy;
              
            }
            
          }
/*********************************************************************************************************
          
          //Do all of the EDF scheduling here

*********************************************************************************************************/          
       
       prev_prior = 5;
       //Temporary store for after processing
       t1_move_exe_tmp =  t1_move.exe;
       t2_rightLed_exe_tmp =  t2_rightLed.exe;  
       t3_leftLed_exe_tmp =  t3_leftLed.exe; 
       
       //if (t1_move.exe == 1 | t2_rightLed.exe == 1 | t3_leftLed.exe == 1 | t4_moveForward.exe == 1 | t5_moveBackward.exe == 1){
       while (root != nil) {
         
         //if (avl_exe_right == 0){min_rdy_q_node = minimum(root);}
         if (avl_exe_right == 0){min_rdy_q_node = min_rdy_ptr_imm;}
         else {min_rdy_q_node = root->right;avl_exe_right =0;}

         min = min_rdy_q_node->value;
    
         //Always assign the highest priority to the KEY = DEADLINE TICK TIME - RELEASE TIME TIME
         if(min_rdy_q_node->n == 5 && t1_move.exe == 1) {
           move_Exe_PRIO = prev_prior;
           iMove   = 10;
           t1_move.exe = 0;
           OSTaskCreate((OS_TCB     *)&AppTaskOneTCB, (CPU_CHAR   *)"1", (OS_TASK_PTR ) AppTaskOne, (void       *) 0, (OS_PRIO     ) move_Exe_PRIO, (CPU_STK    *)&AppTaskOneStk[0], (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *)(CPU_INT32U) 1, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
         } else if(min_rdy_q_node->n == 5 && t2_rightLed.exe == 1) {
           rightLEDBlink_Exe_PRIO = prev_prior;
           t2_rightLed.exe = 0;
           OSTaskCreate((OS_TCB     *)&AppTaskTwoTCB, (CPU_CHAR   *)"2", (OS_TASK_PTR ) AppTaskTwo, (void       *) 0, (OS_PRIO     ) rightLEDBlink_Exe_PRIO, (CPU_STK    *)&AppTaskTwoStk[0], (CPU_STK_SIZE) APP_TASK_TWO_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_TWO_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
         }
 
         
         if(min_rdy_q_node->n == 10 && t3_leftLed.exe == 1) {
           leftLEDBlink_Exe_PRIO = prev_prior;
           OSTaskCreate((OS_TCB     *)&AppTaskThreeTCB, (CPU_CHAR   *)"3", (OS_TASK_PTR ) AppTaskThree, (void       *) 0, (OS_PRIO     ) leftLEDBlink_Exe_PRIO, (CPU_STK    *)&AppTaskThreeStk[0], (CPU_STK_SIZE) APP_TASK_THREE_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_THREE_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 3, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
       
         }
    
         if (BENCHMARK_CODE > 0) {
            StartTime3 = OS_TS_GET();
            diff = StartTime2 - StartTime;
            OverheadValue = StartTime3  - StartTime2 - diff;
         }
         if (t1_move.exe == 1 && t2_rightLed.exe == 1)
            prev_prior = prev_prior + 2;
         else
            prev_prior = prev_prior + 1;
         
         if (tree_size == 2) {
           min_rdy_q_node = root->right;
           tree_size--;
           avl_exe_right = 1;
         }//if it is the last 2, there is a bug. do not delete first
         else if (tree_size == 1) {
           if (root->right != NULL) {
           root->right->parent = NULL;
           root->right->right = NULL;
           root->right->left = NULL;
           root->right->value = 0;
           root->right->n = 0;
           root->right->index = 0;}
           
           root->parent = NULL;
           root->right = NULL;
           root->left = NULL;
           root->value = 0;
           root->n = 0;
           root->index = 0;
           tree_size=0;
           //root = NULL;
           root = nil;
           h[0] = 0;h[1] = 0;h[2] = 0;h[3] = 0;h[4] = 0;h[5] = 0; 
         }//if it is the last node, re-init the root
         //If the min of the minumum has the same value as the root node, we have to delete both the root node and the min node
         else
         { 
           delete_from_rdy_q_node(min_rdy_q_node);
           tree_size--;
         }

       }

       t1_move.exe = t1_move_exe_tmp;
       t2_rightLed.exe = t2_rightLed_exe_tmp;
       t3_leftLed.exe = t3_leftLed_exe_tmp;
       
          //Dispatch all the task at the same time if 2 or more task exe = 1
            if(task_num_exe == 3) {
            t1_move.exe = 0;
            t2_rightLed.exe = 0;
            t3_leftLed.exe = 0;  
            task_num_exe = 0;

            value = 0;
            while (value == 0){
            if (StatusFlags.Flags == 0x7) {value = 1;StatusFlags.Flags = 0x0;}
            }
            OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL + OS_OPT_POST_NO_SCHED,&err);
            
            } else if (task_num_exe == 2 && t1_move.exe == 1 && t2_rightLed.exe == 1){
            t1_move.exe = 0;
            t2_rightLed.exe = 0; 
            task_num_exe = 0;       
            value = 0;
            while (value == 0){
              if (StatusFlags.Flags == 0x3) {value = 1;StatusFlags.Flags = 0x0;}
              }
            //OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL + OS_OPT_POST_NO_SCHED,&err);
            OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL,&err);
            } else if (task_num_exe == 2 && t1_move.exe == 1 && t3_leftLed.exe == 1){
            t1_move.exe = 0;
            t3_leftLed.exe = 0;
            task_num_exe = 0;
             OSFlagPend(&StatusFlags,TASK1_OK + TASK3_OK,OS_OPT_PEND_FLAG_SET_ALL + OS_OPT_PEND_FLAG_CONSUME,0,
                           OS_OPT_PEND_BLOCKING,&err);         
             OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL + OS_OPT_POST_NO_SCHED,&err);
            }  else if (task_num_exe == 2 && t2_rightLed.exe == 1 && t3_leftLed.exe == 1){
            t2_rightLed.exe = 0;
            t3_leftLed.exe = 0;   
            task_num_exe = 0;
            OSFlagPend(&StatusFlags,TASK2_OK + TASK3_OK,OS_OPT_PEND_FLAG_SET_ALL + OS_OPT_PEND_FLAG_CONSUME,0,
                           OS_OPT_PEND_BLOCKING,&err); 
            OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL + OS_OPT_POST_NO_SCHED,&err);             
            } else if (t1_move.exe == 1){
            OSFlagPend(&StatusFlags,TASK1_OK,OS_OPT_PEND_FLAG_SET_ALL + OS_OPT_PEND_FLAG_CONSUME,0,
                           OS_OPT_PEND_BLOCKING,&err); 
            OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL + OS_OPT_POST_NO_SCHED,&err);                
             task_num_exe = 0;
             t1_move.exe = 0;
            } else if (t2_rightLed.exe == 1){
            OSFlagPend(&StatusFlags,TASK2_OK,OS_OPT_PEND_FLAG_SET_ALL + OS_OPT_PEND_FLAG_CONSUME,0,
                           OS_OPT_PEND_BLOCKING,&err); 
            OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL + OS_OPT_POST_NO_SCHED,&err);                
             task_num_exe = 0;
             t2_rightLed.exe = 0;
            } else if (t3_leftLed.exe == 1){
            OSFlagPend(&StatusFlags,TASK3_OK,OS_OPT_PEND_FLAG_SET_ALL + OS_OPT_PEND_FLAG_CONSUME,0,
                           OS_OPT_PEND_BLOCKING,&err); 
            OSSemPost(&SEM_TASK123,OS_OPT_POST_ALL + OS_OPT_POST_NO_SCHED,&err);                
             task_num_exe = 0;
             t3_leftLed.exe = 0;
             
            }
       
    }
    
    //Do not delete this task
}

static  void  AppTaskOne (void  *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  iSec, k, i, j,SECOND_VAL;
    CPU_TS ts;
    CPU_TS diff;
    
    iSec = WORKLOAD1;
    SECOND_VAL = ONESECONDTICK;
    OS_MSG_SIZE msg_size;
    
    //Start posting the flag indicating the task is OK and ready to start to execute
    OSFlagPost(&StatusFlags,TASK1_OK,OS_OPT_POST_FLAG_SET + OS_OPT_POST_NO_SCHED,&err);
    //OSFlagPost(&StatusFlags,TASK1_OK,OS_OPT_POST_FLAG_SET,&err);
    //Added to synchronous release
    OSSemPend(&SEM_TASK123,0,OS_OPT_PEND_BLOCKING,&ts,&err);
 
    OSQPend(&DummyQ, TASK1PERIOD * 200, OS_OPT_PEND_BLOCKING, &msg_size, &ts, &err);

    if (BENCHMARK_CODE > 0) {
          app1_time1 = OS_TS_GET();
          app1_time2 = OS_TS_GET();
    }    
    
    OSMutexPend((OS_MUTEX *)&MutexTwo, (OS_TICK )0, (OS_OPT )OS_OPT_PEND_BLOCKING, (CPU_TS *)&ts, (OS_ERR *)&err);
    
    if (BENCHMARK_CODE > 0) {
          app1_start_mutex2_get_time = OS_TS_GET();
          diff = app1_time2 - app1_time1;
          app1_mutex2_get_overhead = app1_start_mutex2_get_time - app1_time2 - diff;
          app1_time1 = OS_TS_GET();
          app1_time2 = OS_TS_GET();
    }
    
    OSMutexPend((OS_MUTEX *)&MutexThree, (OS_TICK )0, (OS_OPT )OS_OPT_PEND_BLOCKING, (CPU_TS *)&ts, (OS_ERR *)&err);
    
    if (BENCHMARK_CODE > 0) {
          app1_start_mutex3_get_time = OS_TS_GET();
          diff = app1_time2 - app1_time1;
          app1_mutex3_get_overhead = app1_start_mutex3_get_time - app1_time2 - diff;
          app1_time1 = OS_TS_GET();
          app1_time2 = OS_TS_GET();
    }
    
     if(iMove > 0)
    {
      RoboTurn(FRONT, 14, 50);
      iMove--;
    }
    
    for(k=0; k<iSec; k++)
    {
      for(i=0; i <ONESECONDTICK; i++) {
       //  j = ((i * 2) + j);
      }
    }

    BSP_MotorStop(LEFT_SIDE);
    BSP_MotorStop(RIGHT_SIDE);

    if (BENCHMARK_CODE > 0) {
          app1_exe_time_tmp = OS_TS_GET();
          diff = app1_time2 - app1_time1;
          app1_exe_time = app1_exe_time_tmp - app1_time2 - diff;
          app1_time1 = OS_TS_GET();
          app1_time2 = OS_TS_GET();
    }     
  
    OSMutexPost((OS_MUTEX *)&MutexTwo, (OS_OPT )OS_OPT_POST_NONE, (OS_ERR *)&err);
    
    if (BENCHMARK_CODE > 0) {
      app1_start_mutex2_rel_time = OS_TS_GET();
      diff = app1_time2 - app1_time1;
      app1_mutex2_rel_overhead = app1_start_mutex2_rel_time - app1_time2 - diff;
      app1_time1 = OS_TS_GET();
      app1_time2 = OS_TS_GET();      
    }
    
    OSMutexPost((OS_MUTEX *)&MutexThree, (OS_OPT )OS_OPT_POST_NONE, (OS_ERR *)&err);
    if (BENCHMARK_CODE > 0) {
      app1_start_mutex3_rel_time = OS_TS_GET();
      diff = app1_time2 - app1_time1;
      app1_mutex3_rel_overhead = app1_start_mutex3_rel_time - app1_time2 - diff;
    }
    
    //printf("\nT1");
    OSTaskDel((OS_TCB *)0, &err);
}

static  void  AppTaskTwo (void  *p_arg)
{
    OS_ERR      err;
    CPU_INT32U  i, j=7;
    CPU_TS ts;
    CPU_TS diff;

    //Start posting the flag indicating the task is OK and ready to start to execute
    OSFlagPost(&StatusFlags,TASK2_OK,OS_OPT_POST_FLAG_SET + OS_OPT_POST_NO_SCHED,&err);
    //OSFlagPost(&StatusFlags,TASK2_OK,OS_OPT_POST_FLAG_SET,&err);
    
    //Added to synchronous release
    OSSemPend(&SEM_TASK123,0,OS_OPT_PEND_BLOCKING,&ts,&err);

    if (BENCHMARK_CODE > 0) {
          app2_time1 = OS_TS_GET();
          app2_time2 = OS_TS_GET();
    }        
    
    OSMutexPend((OS_MUTEX *)&MutexThree, (OS_TICK )0, (OS_OPT )OS_OPT_PEND_BLOCKING, (CPU_TS *)&ts, (OS_ERR *)&err);

    if (BENCHMARK_CODE > 0) {
          app2_start_mutex3_get_time = OS_TS_GET();
          diff = app2_time2 - app2_time1;
          app2_mutex3_get_overhead = app2_start_mutex3_get_time - app2_time2 - diff;
          app2_time1 = OS_TS_GET();
          app2_time2 = OS_TS_GET();
    }    
      
    
    BSP_LED_On(1u);
    for(i=0; i <(WORKLOAD2*ONESECONDTICK); i++)
    {//BSP_LED_Toggle(1u);
      j = ((i * 2) + j);
    }
    BSP_LED_Off(1u);

    if (BENCHMARK_CODE > 0) {
          app2_exe_time_tmp = OS_TS_GET();
          diff = app2_time2 - app2_time1;
          app2_exe_time = app2_exe_time_tmp - app2_time2 - diff;
          app2_time1 = OS_TS_GET();
          app2_time2 = OS_TS_GET();
    }  
    
    OSMutexPend((OS_MUTEX *)&MutexTwo, (OS_TICK )0, (OS_OPT )OS_OPT_PEND_BLOCKING, (CPU_TS *)&ts, (OS_ERR *)&err);

    if (BENCHMARK_CODE > 0) {
          app2_start_mutex2_get_time = OS_TS_GET();
          diff = app2_time2 - app2_time1;
          app2_mutex2_get_overhead = app2_start_mutex2_get_time - app2_time2 - diff;
          app2_time1 = OS_TS_GET();
          app2_time2 = OS_TS_GET();
    }   
        
    OSMutexPost((OS_MUTEX *)&MutexTwo, (OS_OPT )OS_OPT_POST_NONE, (OS_ERR *)&err);
    
    if (BENCHMARK_CODE > 0) {
      app2_start_mutex2_rel_time = OS_TS_GET();
      diff = app2_time2 - app2_time1;
      app2_mutex2_rel_overhead = app2_start_mutex2_rel_time - app2_time2 - diff;
      app2_time1 = OS_TS_GET();
      app2_time2 = OS_TS_GET();
    }  
    
    OSMutexPost((OS_MUTEX *)&MutexThree, (OS_OPT )OS_OPT_POST_NONE, (OS_ERR *)&err);
    
    if (BENCHMARK_CODE > 0) {
      app2_start_mutex3_rel_time = OS_TS_GET();
      diff = app2_time2 - app2_time1;
      app2_mutex3_rel_overhead = app2_start_mutex3_rel_time - app2_time2 - diff;
    }  
    
    #if(MICROSD_EN == 1) 
    sprintf(g_cCmdBuf,"app log.txt  %d %d %d", j, iCounter++, (OSTimeGet(&err)/1000));
    CmdLineProcess(g_cCmdBuf);
    #endif

    //printf("\nT2");
    OSTaskDel((OS_TCB *)0, &err);
}

static  void  AppTaskThree (void  *p_arg)
{   BSP_DisplayStringDraw("TASK THREE",0u, 0u);
    OS_ERR      err;
    CPU_INT32U  iSec, k, i, j;
    CPU_TS ts;
    CPU_TS diff;
    
    iSec = WORKLOAD3;

    //Start posting the flag indicating the task is OK and ready to start to execute
    OSFlagPost(&StatusFlags,TASK3_OK,OS_OPT_POST_FLAG_SET + OS_OPT_POST_NO_SCHED,&err);
    //OSFlagPost(&StatusFlags,TASK3_OK,OS_OPT_POST_FLAG_SET,&err);
    //Added to synchronous release
    OSSemPend(&SEM_TASK123,0,OS_OPT_PEND_BLOCKING,&ts,&err);

    if (BENCHMARK_CODE > 0) {
          app3_time1 = OS_TS_GET();
          app3_time2 = OS_TS_GET();
    }  
        
    OSMutexPend((OS_MUTEX *)&MutexOne, (OS_TICK )0, (OS_OPT )OS_OPT_PEND_BLOCKING, (CPU_TS *)&ts, (OS_ERR *)&err);

    if (BENCHMARK_CODE > 0) {
       app3_start_mutex1_get_time = OS_TS_GET();
       diff = app3_time2 - app3_time1;
       app3_mutex1_get_overhead = app3_start_mutex1_get_time - app3_time2 - diff;
       app3_time1 = OS_TS_GET();
       app3_time2 = OS_TS_GET();
    }    
    
    BSP_LED_On(2u);

    for(k=0; k<iSec; k++)
    {
      //BSP_LED_Toggle(2u);
      for(i=0; i <ONESECONDTICK; i++)
         j = ((i * 2) + j);
    }
    BSP_LED_Off(2u);
  //  BSP_DisplayClear();
    if (BENCHMARK_CODE > 0) {
          app3_exe_time_tmp = OS_TS_GET();
          diff = app3_time2 - app3_time1;
          app3_exe_time = app3_exe_time_tmp - app3_time2 - diff;
          app3_time1 = OS_TS_GET();
          app3_time2 = OS_TS_GET();
    }      
    
    OSMutexPost((OS_MUTEX *)&MutexOne, (OS_OPT )OS_OPT_POST_NONE, (OS_ERR *)&err);

    if (BENCHMARK_CODE > 0) {
      app3_start_mutex1_rel_time = OS_TS_GET();
      diff = app3_time2 - app3_time1;
      app3_mutex1_rel_overhead = app3_start_mutex1_rel_time - app3_time2 - diff;
    }
    
    OSTaskDel((OS_TCB *)0, &err);
}


void IntWheelSensor()
{
	CPU_INT32U         ulStatusR_A;
	CPU_INT32U         ulStatusL_A;

	static CPU_INT08U CountL = 0;
	static CPU_INT08U CountR = 0;

	static CPU_INT08U data = 0;

	ulStatusR_A = GPIOPinIntStatus(RIGHT_IR_SENSOR_A_PORT, DEF_TRUE);
	ulStatusL_A = GPIOPinIntStatus(LEFT_IR_SENSOR_A_PORT, DEF_TRUE);

        if (ulStatusR_A & RIGHT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(RIGHT_IR_SENSOR_A_PORT, RIGHT_IR_SENSOR_A_PIN);           /* Clear interrupt.*/
          CountR = CountR + 1;
        }

        if (ulStatusL_A & LEFT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(LEFT_IR_SENSOR_A_PORT, LEFT_IR_SENSOR_A_PIN);
          CountL = CountL + 1;
        }

	if((CountL >= Left_tgt) && (CountR >= Right_tgt))
        {
          data = 0x11;
          Left_tgt = 0;
          Right_tgt = 0;
          CountL = 0;
          CountR = 0;
          BSP_MotorStop(LEFT_SIDE);
          BSP_MotorStop(RIGHT_SIDE);
        }
        else if(CountL >= Left_tgt)
        {
          data = 0x10;
          Left_tgt = 0;
          CountL = 0;
          BSP_MotorStop(LEFT_SIDE);
        }
        else if(CountR >= Right_tgt)
        {
          data = 0x01;
          Right_tgt = 0;
          CountR = 0;
          BSP_MotorStop(RIGHT_SIDE);
        }
        return;
}


static  void  AppRobotMotorDriveSensorEnable ()
{
    BSP_WheelSensorEnable();
    BSP_WheelSensorIntEnable(RIGHT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
    BSP_WheelSensorIntEnable(LEFT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
}

/*
static  void  AppRobotMotorDriveSensorDisable ()
{
    BSP_WheelSensorDisable();
    BSP_WheelSensorIntDisable(RIGHT_SIDE, SENSOR_A);
    BSP_WheelSensorIntDisable(LEFT_SIDE, SENSOR_A);
}
*/
void RoboTurn(tSide dir, CPU_INT16U seg, CPU_INT16U speed)
{
	Left_tgt = seg;
        Right_tgt = seg;

	BSP_MotorStop(LEFT_SIDE);
	BSP_MotorStop(RIGHT_SIDE);

        BSP_MotorSpeed(LEFT_SIDE, speed <<8u);
	BSP_MotorSpeed(RIGHT_SIDE,speed <<8u);

	switch(dir)
	{
            case FRONT :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case BACK :
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            case LEFT_SIDE :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case RIGHT_SIDE:
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            default:
                    BSP_MotorStop(LEFT_SIDE);
                    BSP_MotorStop(RIGHT_SIDE);
                    break;
	}

	return;
}

#if(TIMER_EN == 1)
unsigned long TimerTick(void)
{
    return((TimerValueGet(TIMER3_BASE, TIMER_A)/TIMERDIV));
}

void TimerReset(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
    SysCtlPeripheralReset(SYSCTL_PERIPH_TIMER3);
    TimerConfigure(TIMER3_BASE, TIMER_CFG_32_BIT_PER_UP);
    TimerLoadSet(TIMER3_BASE, TIMER_BOTH, 0xffffffff);
    TimerEnable(TIMER3_BASE, TIMER_BOTH);
}
#endif

/*
*********************************************************************************************************
*                                                
*                                C++ Fucntion to Implement Fibonacci Heap
*
*********************************************************************************************************
*/

/*
 * Insert Node
 */

struct node* Insert(struct node* H, struct node* x)
{
    x->degree = 0;
    x->parent = NULL;
    x->child = NULL;
    x->left = NULL;
    x->right = NULL;

    if (H != NULL)
    {
        (H->left)->right = x;
        x->right = H;
        x->left = H->left;
        H->left = x;
        if (x->n < H->n)
            H = x;
    }
    else
    {
        H = x;
    }
    nH = nH + 1;
    return H;
}

/*
 * Extract Min Node in Fibonnaci Heap
 */
struct node* Extract_Min(struct node* x)
{
	
    //The min is always locate at the root list
    CPU_INT32U  min_fibo;
    struct node* p;
    struct node* x_tmp;
    struct node* H_tmp;
    struct node* x_org; //store  the orignal content
    /*
    //finding the minimum remain_ctr
    min = t1_move.remain_ctr;
    p = &t1_move;
    
    if (t2_rightLed.remain_ctr < min) {
          min = t2_rightLed.remain_ctr;
          p = &t2_rightLed;
    }

    if (t3_leftLed.remain_ctr < min) {
          min =t3_leftLed.remain_ctr;
          p = &t3_leftLed;
    }
    */
    x_org = x;
    min_fibo = x->remain_ctr;
    p = x;
    x_tmp = x; //make hard copy first
    H_tmp = x;//make hard copy first
    //Compare untill the left most node in the root list
    while (H_tmp != NULL) {
      if (H_tmp->left->remain_ctr < min_fibo){
        min_fibo = H_tmp->left->remain_ctr; 
        p = H_tmp->left;
      } else {
      H_tmp = H_tmp->left;
      }
    }

    //Compare untill the right most node in the root list
    while (x_tmp != NULL) {
      if (x_tmp->right->remain_ctr < min_fibo){
        min_fibo = x_tmp->right->remain_ctr; 
        p = x_tmp->right;
      } else {
      x_tmp = x_tmp->right;
      }
    }
    
    x = x_org;
    
    return p;
}

void Fibo_Sort(struct node* x)
{
  CPU_INT32U nH_tmp;
  CPU_INT32U degree_tmp;
  struct node* ptr_left_most;
  struct node* ptr_left_most_tmp;
  struct node* ptr_tmp;
  struct node* ptr_tmp2;
  struct node* ptr_tmp3;
  struct node* node_deg0;
  struct node* prev_node_deg0;
  struct node* node_deg_tmp;
  struct node* node_deg_tmp2;
  CPU_INT32U node_deg_in_between = 0;
  CPU_INT32U node_deg0_cnt = 0;
  CPU_INT32U num_root_node =0;
  CPU_INT32U result_main;
  CPU_INT32U result_right; 

  
  nH_tmp = nH;

  //First check the degree of each node, 
  //0 means no child, 
  //if 2 nodes have the same degree, perform merging
  //Always the pointer is the min,so it is always the parent node,no parent above
  //Loop until a NULL is reach
  //check that the delete node has no child
    if(x->degree == 0) {
      
      //check if there is any parent for this node
      if (x->parent != NULL) {
            x->parent->degree = 0;
            x->parent->child = NULL;
            x->parent = NULL;
            x->child  = NULL;              
      } else {  
            x->parent = NULL;
            x->child  = NULL;    
      }
      //check and update the left of the deleted node
      if (x->left != NULL & x->right == NULL) {
        //copy the orignial deleted node to ptr_left_most;
        ptr_left_most =  (x->left);
        //perform update on the right of the left node to NULL
        (x->left)->right = NULL;    

      } else
      
      //check and update the right of the deleted node
      if (x->right != NULL & x->left == NULL) {
        //copy the orignial deleted node to ptr_left_most;
        ptr_left_most =  (x->right);
        //perform update on the left of the right node to NULL
        (x->right)->left = NULL;       
      } else
      
      //check and update the right of the deleted node
      if (x->right != NULL & x->left != NULL) {
        //copy the orignial deleted node to ptr_left_most;
        ptr_left_most =  (x->left);
        //perform connection of x->right to x->left
        (x->right)->left = (x->left);  
        (x->left)->right = (x->right);
        
      }
      x->right = NULL;
      x->left = NULL;    
    }
    else //if there is a child under the deleted node
    {
      //make all child node become root node
      //hard coded as thou as there is only 5 task at this moment
      //and there is no left and right node of the deleted task
      (x->child)->parent = NULL;
      (x->child->left)->parent = NULL;
      (x->child->right)->parent = NULL;
    
     
      //if the left and right of the child node is not a NULL
      if ((x->child)->left != NULL & (x->child)->right != NULL) {
              (x->child->left)->left = x->left;
              (x->child->right)->right = x->right;
              //Make connection of the root list to the child node
              (x->left)->right = (x->child)->left;
              (x->right)->left = (x->child)->right;
              //copy the orignial child of the  deleted node ;
              ptr_left_most =  x->child;
      } else//if the left of the child node is not a NULL but right is a NULL
        if ((x->child)->left != NULL & (x->child)->right == NULL) {
              (x->child->left)->left = x->left;
              (x->child)->right = x->right;
              //Make connection of the root list to the child node
              (x->left)->right = (x->child)->left;
              (x->right)->left = x->child;            
              //copy the orignial child of the  deleted node ;
              ptr_left_most =  x->child;
        } else//if the right of the child node is not a NULL but left is a NULL
          if ((x->child)->left == NULL & (x->child)->right != NULL) {
                (x->child)->left = x->left;   
                (x->child->right)->right = x->right;
                //Make connection of the root list to the child node
                (x->left)->right = x->child;
                (x->right)->left = (x->child)->right;  
                //copy the orignial child of the  deleted node ;
                ptr_left_most =  x->child;
          } else//if the left right of the child node is a NULL
            if ((x->child)->left == NULL & (x->child)->right == NULL) {
                (x->child)->left  = x->left;   
                (x->child)->right = x->right; 
                //Make connection of the root list to the child node
                (x->left)->right = x->child;
                (x->right)->left = x->child; 
                //copy the orignial child of the  deleted node ;
                ptr_left_most =  x->child;
            }
      
       //x->degree = x->degree  - 1;
      //Since the node is deleted from the list , the degree will be assign with 0
       x->degree = 0;
       x->right = NULL;
       x->left = NULL; 
       x->child = NULL; 
      
    }
    
    nH = nH - 1; //decrement 1 node
    nH_tmp = nH;
    
    //Must update the H -pointer to the latest min in the root list
    if (ptr_left_most != NULL){//execute only when ptr_left_most is not a NULL
      H = Extract_Min(ptr_left_most);
    
    
    //Perform Sorting after deletion, and added back the deleted node
    // Sorting base on remain_ctr
      
    //first rearrange all of the that is the same degree in 1 group
    //from smallest degree starting from left most and increasing to the right
    while (ptr_left_most->left != NULL) {
      ptr_left_most = ptr_left_most->left; 
    }      
    
    num_root_node = 0;
    ptr_left_most_tmp = ptr_left_most;
     while (ptr_left_most->right != NULL) {
      ptr_left_most = ptr_left_most->right;
      num_root_node = num_root_node + 1;//count number of node that is in the root list
    }      
    ptr_left_most = ptr_left_most_tmp;
    //First sort out all node that is degree 0, from left most till right at this moment first
    node_deg0_cnt = 0;
    node_deg_in_between = 0;
    num_root_node = num_root_node + 1; //+1 until the very last node
    prev_node_deg0 = NULL;
    prev_node_deg0->right = NULL;
    prev_node_deg0->left = NULL;
    while (num_root_node != 0) {
      
      if (ptr_left_most->degree == 0 & node_deg0_cnt == 0){
          prev_node_deg0 = ptr_left_most;
          node_deg0_cnt = 1;
      } else if (ptr_left_most->degree == 0 & node_deg0_cnt == 1 & ptr_left_most->left != prev_node_deg0 & ptr_left_most->left == prev_node_deg0->right & node_deg_in_between !=0) {
        //make a copy first of the middle node in between
        node_deg_tmp = ptr_left_most->left;
        node_deg_tmp2 = ptr_left_most->right;
        //making swap connection
        prev_node_deg0->right = ptr_left_most;
        ptr_left_most->left = prev_node_deg0;
        ptr_left_most->right = node_deg_tmp;
        node_deg_tmp->left = ptr_left_most;
        node_deg_tmp->right = node_deg_tmp2;
          
        node_deg0_cnt = 0; 
      } else {node_deg_in_between = ptr_left_most->degree;}
      
      ptr_left_most = ptr_left_most->right; 
      num_root_node = num_root_node - 1;
    } 
      
    
    //Finding the leftmost node untill a NULL is found from the left, starting from the the left
    //or right node from the previous deleted node
    //while (ptr_left_most->left != NULL) {
     // ptr_left_most = ptr_left_most->left; 
    //}
    ptr_left_most = ptr_left_most_tmp;
    //no sorting will be perform if not both of the remain_ctr is the same
    //Start Sorting starting from ptr_left_most, untill a NULL is found from the right of the ptr_left_most
    ptr_tmp2 = ptr_left_most;
    //while (ptr_tmp2->right != NULL) {
    while (ptr_tmp2 != NULL) {
        //if (ptr_left_most->degree ==  (ptr_left_most->right)->degree){
        if (ptr_tmp2->degree ==  (ptr_tmp2->right)->degree){
            //start a child note ,
            //store result to synchronize the number counter
          if (ptr_tmp2->exe == 1) {result_main = ptr_tmp2->remain_ctr + 1;} 
          else {result_main = ptr_tmp2->remain_ctr;}
          
          if ((ptr_tmp2->right)->exe == 1) {result_right = (ptr_tmp2->right)->remain_ctr + 1;}
          else {result_right = (ptr_tmp2->right)->remain_ctr;}
          
          //ptr_tmp = (ptr_tmp2->right)->right;//store the pointing at the right node of the ptr_tmp2->right
          
          //if (ptr_left_most->remain_ctr < (ptr_left_most->right)->remain_ctr) {
          if (result_main < result_right) {
             //make a full copy of the ptr_tmp2->right instance first
              ptr_tmp = (ptr_tmp2->right)->right;//store a copy of right node of the  ptr_tmp2 first
             
             //make ptr_tmp2->right a child node of ptr_tmp2
             //check if the child node is not a NULL first
              if (ptr_tmp2->child == NULL) {
                 ptr_tmp2->child = ptr_tmp2->right;
                (ptr_tmp2->right)->parent = ptr_tmp2;
                (ptr_tmp2->right)->left = NULL;
                (ptr_tmp2->right)->right = NULL;
              } else {
                //inserted as child 
                //and make connection to among the child node
                (ptr_tmp2->child)->left = ptr_tmp2->right;
                (ptr_tmp2->right)->right = ptr_tmp2->child;
                
                 ptr_tmp2->child = ptr_tmp2->right;
                (ptr_tmp2->right)->parent = ptr_tmp2;
                (ptr_tmp2->right)->left = NULL;
                                              
              }
             
             
            //connect the right of the child node to the ptr_tmp2
            //make ptr_tmp2 or left most as parent
            ptr_tmp2->right = ptr_tmp;
            ptr_tmp2->degree = ptr_tmp2->degree + 1;
            cur_degree =  ptr_tmp2->degree;
            
            if (ptr_tmp != NULL){ //assign only if the ptr_tmp is not a NULL
              ptr_tmp->left = ptr_tmp2;
            }
            
            
            //Check if the sorted degree is the same as from the previous one or not
            //If is the same, start from the leftmost position again
           // if (ptr_tmp2->degree == prev_degree) {
              if (cur_degree == prev_degree) {
                ptr_left_most = ptr_tmp2;
                while (ptr_left_most->left != NULL) {
                  ptr_left_most = ptr_left_most->left; 
                }
                ptr_tmp2 = ptr_left_most;//start from the beginining again                  
          } else { //starting from itself again
            /*
            if (ptr_tmp == NULL) {
              ptr_tmp2->right = NULL;
            } else {
              ptr_tmp2 = ptr_tmp;
            } */
            ptr_tmp2 = ptr_tmp2;
            //record the previous highest sorting degree
            prev_degree = cur_degree;
            }
          
            
          } 
          else if (result_main > result_right)
          { //ptr_left_most->right is less than ptr_left_most
            //To be done
             
            //make a full copy of the ptr_tmp2 instance first
             ptr_tmp = ptr_tmp2->right;//store the right node reference of ptr_tmp2 first
             
             if ((ptr_tmp2->right)->child == NULL) { //Check if the ptr_tmp2->right child node is a NULL or not
            //make ptr_tmp2 as child node
            (ptr_tmp2->right)->child = ptr_tmp2;         
            //connect to the left node of the ptr_tmp2
            (ptr_tmp2->right)->left = ptr_tmp2->left;//connect to the left node of the ptr_tmp2
            (ptr_tmp2->left)->right = ptr_tmp2->right; 
            
            (ptr_tmp2->right)->degree = (ptr_tmp2->right)->degree + 1;
            cur_degree =  (ptr_tmp2->right)->degree;
           
            //make ptr_tmp2 as child node
            ptr_tmp2->parent = ptr_tmp2->right;
            ptr_tmp2->left = NULL;
            ptr_tmp2->right = NULL;
             } else {
                // TO BE LED  FEWAFEWAFAWFAEWFWEFWEFWAFWEFWAFWEFWEFAWFWAEFFEW
                //inserted as child 
                //and make connection to among the child node
                //make a copy first, of all of the related notes
                ptr_tmp3 = (ptr_tmp2->right);
               
                (ptr_tmp2->right->child)->left = ptr_tmp2;            
                ptr_tmp2->right = ptr_tmp2->right->child;
                
                //cannot use ptr_tmp2->right anymore onward, must use all of the copies
                
                //make ptr_tmp2 as child node
                //(ptr_tmp2->right)->child = ptr_tmp2;   
                ptr_tmp3->child = ptr_tmp2; 
                //connect to the left node of the ptr_tmp2
                //(ptr_tmp2->right)->left = ptr_tmp2->left;//connect to the left node of the ptr_tmp2
                ptr_tmp3->left = ptr_tmp2->left;
                //(ptr_tmp2->left)->right = ptr_tmp2->right; 
                (ptr_tmp2->left)->right = ptr_tmp3;
            
                ptr_tmp3->degree = ptr_tmp3->degree + 1;
                cur_degree =  ptr_tmp3->degree;
           
                //make ptr_tmp2 as child node
                ptr_tmp2->parent = ptr_tmp3;
                ptr_tmp2->left = NULL;
                   
                              
             }
                    
            //Check if the sorted degree is the same as from the previous one or not
            //If is the same, start from the leftmost position again
            //if (ptr_tmp->degree == prev_degree) {
            if (cur_degree == prev_degree) {
                ptr_left_most = ptr_tmp;
                while (ptr_left_most->left != NULL) {
                  ptr_left_most = ptr_left_most->left; 
                }
                ptr_tmp2 = ptr_left_most;//start from the beginining again
            } else {//else resume the next right
            if (ptr_tmp != NULL) {  //assign only if it is not a NULL
                ptr_tmp2 = ptr_tmp;
            } else {
                //ptr_tmp2->right = NULL;
                ptr_tmp2 = NULL;
            }
            //record the previous highest sorting degree
            prev_degree = cur_degree;
            }
            
            
          } else //both are equal
          {
            //no action done here, only shift
            ptr_tmp2 = ptr_tmp2->right;
          }
              
     
        } else {
            
          ptr_tmp2 = ptr_tmp2->right;
        }
    }

    }
   return;
}


/*
*********************************************************************************************************
*                                                
*                                C++ Fucntion to Implement AVL
*
*********************************************************************************************************
*/

struct rdy_q_node* init_tree() {
    //struct rdy_q_node avl_null_node;

	avl_null_node.left  = NULL;
        avl_null_node.right = NULL;
        avl_null_node.parent = NULL;
	avl_null_node.index = 0;
        tree_size = 0;
        return &avl_null_node;
	//nil = &x;
        //root = nil;
   
}

// does a single left rotation
void left_rotate(struct rdy_q_node * x) {
    struct rdy_q_node * y = x->right;
    x->right = y->left;
    if (y->left != nil) {
        y->left->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nil) {
        root = y;
    } else if (x == x->parent->left) {
        x->parent->left = y;
    } else {
        x->parent->right = y;
    }
    y->left = x;
    x->parent = y;
    
        return;
}

// does a single right rotation
void right_rotate(struct rdy_q_node * x) {
    struct rdy_q_node * y = x->left;
    x->left = y->right;
    if (y->right != nil) {
        y->right->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nil) {
        root = y;
    } else if (x == x->parent->right) {
        x->parent->right = y;
    } else {
        x->parent->left = y;
    }
    y->right = x;
    x->parent = y;
    
        return;
}

CPU_INT32U maximum(CPU_INT32U a, CPU_INT32U b) {

    
    if (a > b) return a;

    if (b > a) return b;
    

}

// returns the height of the subtree with root x
CPU_INT32U height(struct rdy_q_node * x) {
    if (x == nil) return 0;
    return maximum(height(x->left),height(x->right))+1;
}

// balances the tree after insert from leaves to the root
// currently you could observe that there is a problem in this code:
// we traverse the tree and begin balacing from every leaf when that's not actually neeeded
// instead we could keep a reference to the leaf node we have added and balance the tree upwards. The author leaves this to the reader
void balance(struct rdy_q_node * x) {
    if (x == nil) return;
    balance(x->left);
    balance(x->right);
    CPU_INT32U h1 = height(x->left);
    h[x->left->index] = h1;
    CPU_INT32U h2 = height(x->right);
    h[x->right->index] = h2;
    if (h1 - h2 == 2) {
        CPU_INT32U lh = h[x->left->left->index];
        CPU_INT32U rh = h[x->left->right->index];
        if (lh-rh == 1) {
            right_rotate(x);
        } else {
            // zig zag case
            left_rotate(x->left);
            right_rotate(x);
        }
    } else if (h1 - h2 == -2) {
        CPU_INT32U lh = h[x->right->left->index];
        CPU_INT32U rh = h[x->right->right->index];
        if (lh-rh == -1) {
            left_rotate(x);
        } else {
            // zig zag case
            right_rotate(x->right);
            left_rotate(x);
        }
    }
    
        return;
}

//void add_to_rdy_q_node(CPU_INT32U a, CPU_INT32U n) {
void add_to_rdy_q_node(struct rdy_q_node* tx_rdy,CPU_INT32U a, CPU_INT32U n) {
    struct rdy_q_node * root_tmp = root;
    struct rdy_q_node * y = nil;
    struct rdy_q_node new_node;
    struct rdy_q_node * z;

    tx_rdy->left = tx_rdy->right = tx_rdy->parent = nil;
    tx_rdy->index = ++tree_size;
    tx_rdy->n = n;
    tx_rdy->value = a;

    z = tx_rdy;
    while (root_tmp != nil) {
        y = root_tmp;
        if (z->value < root_tmp->value) {
            root_tmp = root_tmp->left;
        } else {
            root_tmp = root_tmp->right;
        }
    }
    z->parent = y;
    if (y == nil) {
        root = z;
    } else if(z->value < y->value) {
        y->left = z;
    } else {
        y->right = z;
    }
    balance(root);
    
        return;
}

// in-order print
/*
void print(struct rdy_q_node * el, CPU_INT32U i) {
    if (el->left != nil) print(el->left,i);
    if (el->right != nil) print(el->right,i);
    
        return;
}
*/

struct rdy_q_node* search(CPU_INT32U x) {
    //struct rdy_q_node * temp = new rdy_q_node[1];
    struct rdy_q_node * temp;
    temp = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    
    temp = root;
    while (true) {
        if (temp == nil) {
            return nil;
        }
        if (temp->value == x) {
            return temp;
        }
        if (temp->value < x) {
            temp = temp->right;
            continue;
        }
        if(temp->value > x) {
            temp = temp->left;
            continue;
        }
    }
}

void transplant(struct rdy_q_node * node1,struct  rdy_q_node * node2) {
    if (node1->parent == nil) {
        root = node2;
    } else if (node1->parent->left == node1) {
        node1->parent->left = node2;
    } else {
        node1->parent->right = node2;
    }
    node2->parent = node1->parent;
    
        return;
}

struct rdy_q_node* minimum(struct rdy_q_node * node) {
    while (node->left != nil) node = node->left;
    return node;
}

void delete_balance(struct rdy_q_node * x) {
    CPU_INT32U h1 = height(x->left);
    h[x->left->index] = h1;
    CPU_INT32U h2 = height(x->right);
    h[x->right->index] = h2;

    if (h1-h2 == 2) {
        if (h[x->left->left->index]-h[x->left->right->index] == -1) {
            // zig zag cas
            left_rotate(x->left);
        }
        right_rotate(x);
    } else if (h1-h2 == -2) {
        if (h[x->right->left->index]-h[x->right->right->index] == 1) {
            // zig zag case
            right_rotate(x->right);
        }
        left_rotate(x);
    }
    if (x == root) return;
    delete_balance(x->parent);
    
        return;
}

//void delete_from_rdy_q_node(CPU_INT32U val) {
void delete_from_rdy_q_node(struct rdy_q_node * x) {
   // struct rdy_q_node * x = new rdy_q_node[1];
   // struct rdy_q_node * y = new rdy_q_node[1];
   // struct rdy_q_node * q = new rdy_q_node[1];
    //struct rdy_q_node * x;
    struct rdy_q_node * y;
    struct rdy_q_node * q;   
    //x = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    y = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    q = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    
    //x = search(val);
    if (x == nil) {
        return;
    }
    q = x;
    if (x->left == nil) {
        transplant(x,x->right);
    } else if (x->right == nil) {
        transplant(x,x->left);
    } else {
        y = minimum(x->right);
        if (y->parent != x) {
            transplant(y,y->right);
            y->right = x->right;
            y->right->parent = y;
        }
        transplant(x,y);
        y->left = x->left;
        y->left->parent = y;
    }
    delete_balance(q);
    
    min_rdy_ptr_imm = minimum(root);//find the next minimum;
    
        return;
}

void  APP_TickListInit (void)
{
    int  i;
    static struct app_tick_spoke     *p_spoke;

    for (i = 0u; i < APP_TICK_WHEEL_SIZE; i++) {
        p_spoke                = &APPCfg_TickWheel[i];
        p_spoke->FirstPtr      = NULL;
        p_spoke->NbrEntries    = 0;
        p_spoke->NbrEntriesMax = 0;
    }
    
    return;
}

void  APP_TickListInsert (struct node *task_node,int app_sec)
{
  int match_val,spoke;
  int index_tick_wheel;
  struct  app_tick_spoke *p_spoke;
  
  match_val = app_sec + task_node->n; //APP_SEC + task->n/period
  index_tick_wheel = match_val%APP_TICK_WHEEL_SIZE;//tick spoke value for this task_node
  
  spoke   = index_tick_wheel;
  p_spoke = &APPCfg_TickWheel[spoke];//point to the tick wheel array spoke
  
  if (p_spoke->NbrEntries == 0){
      p_spoke->NbrEntries    = p_spoke->NbrEntries + 1; //Increment the no of entry for the dedicated spoke
      p_spoke->NbrEntriesMax = p_spoke->NbrEntriesMax + 1; //Increment the max no of entry for the dedicated spoke
      p_spoke->FirstPtr      = task_node;               //point the first entry to the first task node
      
      //Update the NON_ZERO_SPOKE number array
      NON_ZERO_SPOKE[NON_ZERO_SPOKE_NUM] = spoke;
      NON_ZERO_SPOKE_NUM = NON_ZERO_SPOKE_NUM + 1;
      
  } else {
      p_spoke->NbrEntries    = p_spoke->NbrEntries + 1; //Increment the no of entry for the dedicated spoke
      p_spoke->NbrEntriesMax = p_spoke->NbrEntriesMax + 1; //Increment the max no of entry for the dedicated spoke    
  }
  
  return;
}

void  APP_TickListRemove (int spoke)
{
    struct  app_tick_spoke *p_spoke;

    //Assume that every next match counter value are all the same in the tick spoke
    //so ticklistremove will remove the first ptr in the dedicated tick spoke
    p_spoke = &APPCfg_TickWheel[spoke];//point to the tick wheel array spoke
    p_spoke->NbrEntries    = 0; 
    p_spoke->NbrEntriesMax = 0; 
    p_spoke->FirstPtr      = NULL;  
    
    //Update the NON_ZERO_SPOKE number array and reset to 0, start from the begining
    for(int i = 0; i<= APP_TICK_WHEEL_SIZE -1; i++) {
      if (NON_ZERO_SPOKE[i] == spoke) {
        NON_ZERO_SPOKE[i] = 0;
        NON_ZERO_SPOKE_NUM = NON_ZERO_SPOKE_NUM - 1;
      }   
    }
}
