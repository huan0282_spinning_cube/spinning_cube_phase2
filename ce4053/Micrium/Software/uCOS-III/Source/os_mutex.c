/*
************************************************************************************************************************
*                                                      uC/OS-III
*                                                 The Real-Time Kernel
*
*                                  (c) Copyright 2009-2011; Micrium, Inc.; Weston, FL
*                           All rights reserved.  Protected by international copyright laws.
*
*                                                   MUTEX MANAGEMENT
*
* File    : OS_MUTEX.C
* By      : JJL
* Version : V3.02.00
*
* LICENSING TERMS:
* ---------------
*           uC/OS-III is provided in source form for FREE short-term evaluation, for educational use or 
*           for peaceful research.  If you plan or intend to use uC/OS-III in a commercial application/
*           product then, you need to contact Micrium to properly license uC/OS-III for its use in your 
*           application/product.   We provide ALL the source code for your convenience and to help you 
*           experience uC/OS-III.  The fact that the source is provided does NOT mean that you can use 
*           it commercially without paying a licensing fee.
*
*           Knowledge of the source code may NOT be used to develop a similar product.
*
*           Please help us continue to provide the embedded community with the finest software available.
*           Your honesty is greatly appreciated.
*
*           You can contact us at www.micrium.com, or by phone at +1 (954) 217-2036.
************************************************************************************************************************
*/

#include <os.h>
#include <includes.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <cstdlib>

#ifdef VSC_INCLUDE_SOURCE_FILE_NAMES
const  CPU_CHAR  *os_mutex__c = "$Id: $";
#endif


#if OS_CFG_MUTEX_EN > 0u

  #define MAX 4
  #define MIN 2

    CPU_TS_TMR Mutex_StartTime;
    CPU_TS_TMR Mutex_StartTime2;        
    CPU_TS_TMR Mutex_OverheadValue;   
    CPU_TS_TMR Mutex_OverheadValue_tmp;

/*
*********************************************************************************************************
*                                            LOCAL VARIABLES For Mutex
*********************************************************************************************************
*/

struct mutex_node  //mutex_node for splay tree implementation
{
    struct mutex_node* l;
    struct mutex_node* r;
    struct mutex_node* p;
    OS_TCB  *OS_TCB_MUTEX_OWNER;
    int v;//mutex identification
    int mutex_id; //use by mutex1_preemp_node to id
    int task_preemp_lvl;
};

struct block_node  //mutex_node for 2-3-4 tree implementation
{
    struct block_node* l;
    struct block_node* r;
    struct block_node* p;
};

struct btreeNode { // use for block  mutex node
        int val[MAX + 1], count;//value is key to store the preemption lvl of the task
        struct btreeNode *link[MAX + 1];
        OS_TCB  *OS_TCB_BLOCK[MAX + 1];
        OS_MUTEX  *MUTEX_BLOCK_PTR[MAX + 1];
  };

struct btreeNode   *root_btree;
struct btreeNode   mutex1_node_block;//Created a physical node
struct btreeNode   mutex2_node_block;//Created a physical node
struct btreeNode   mutex3_node_block;//Created a physical node
struct btreeNode   mutexX_node_block;//Created a physical node

struct mutex_node* splay_root;//Created a root ptr, always pointed to the root node,as identification as a key
struct mutex_node* splay_preemp_root;//Created a root ptr that is base on preemption level as a key,another Root tree stucture
struct mutex_node   splay_nil_root;//Created a null node
struct mutex_node   mutex1_node;//Created a physical node
struct mutex_node   mutex2_node;//Created a physical node
struct mutex_node   mutex3_node;//Created a physical node

struct mutex_node   mutex1_preemp_node;//Created a physical node, the v for these node is preemption lvl
struct mutex_node   mutex2_preemp_node;//Created a physical node, the v for these node is preemption lvl
struct mutex_node   mutex3_preemp_node;//Created a physical node, the v for these node is preemption lvl

int sys_ceiling=4;//System ceiling
int init_splay_tree = 0; //Just to have first initialization of the splay tree root node
char mutex_character;

OS_TCB  *OS_TCB_BLOCK1;
OS_TCB  *OS_TCB_BLOCK2;
OS_TCB  *OS_TCB_BLOCK3;

OS_TCB  *OS_TCB_MUTEX_PRE_OWNER;
struct mutex_node* MUTEX_PRE_OWN;
int mutex_pre_own = 0; 

int block_list; //Temporary store the block list,
                //block_list = 0x1 = Task 1 block
                //block_list = 0x2 = Task 2 block
                //block_list = 0x4 = Task 3 block
                //block_list = 0x7 = Task 1,2,3 block
int TASK_PREEMP;

/*
*********************************************************************************************************
*                                            LOCAL Function
*********************************************************************************************************
*/

void splay_Insert(int v, struct mutex_node* mutex123_node);
void splay_preemp_Insert(int v, struct mutex_node* mutex123_node);

struct mutex_node* Find(int v);
struct mutex_node* Find_preemp(int v);

void Splay(struct mutex_node *T);
void Splay_preemp(struct mutex_node *T);

//int Erase(int v);
int Erase_preemp(int v,int mutex_id);

int Inorder(struct mutex_node *R);
void leftRotate(struct mutex_node *P);
void rightRotate(struct  mutex_node *P);

struct mutex_node* splay_preemp_max(struct mutex_node* node);

struct btreeNode * createNode(int val,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR,struct btreeNode* mutex_node_block, struct btreeNode *child);
void addValToNode(int val, int pos, struct btreeNode *node,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR,struct btreeNode *child);
void splitNode (int val, int *pval, int pos, struct btreeNode *node,struct btreeNode *child,OS_TCB* OSTCBCurPtr, struct btreeNode **newNode);
int setValueInNode(int val, int *pval,struct btreeNode *node,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR, struct btreeNode **child);
void insertion(int val,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR,struct btreeNode* mutex_node_block);
 
void copySuccessor(struct btreeNode *myNode, int pos);
void removeVal(struct btreeNode *myNode, int pos);
void doRightShift(struct btreeNode *myNode, int pos);
void doLeftShift(struct btreeNode *myNode, int pos);
void mergeNodes(struct btreeNode *myNode, int pos);
void adjustNode(struct btreeNode *myNode, int pos);
int delValFromNode(int val, struct btreeNode *myNode);
void deletion(int val, struct btreeNode *myNode);
struct btreeNode* searching(int val, int pos, struct btreeNode *myNode);
//struct btreeNode* max_234_tree_search(struct btreeNode* node);
//void traversal(struct btreeNode *myNode);

/*
************************************************************************************************************************
*                                                   CREATE A MUTEX
*
* Description: This function creates a mutex.
*
* Arguments  : p_mutex       is a pointer to the mutex to initialize.  Your application is responsible for allocating
*                            storage for the mutex.
*
*              p_name        is a pointer to the name you would like to give the mutex.
*
*              p_err         is a pointer to a variable that will contain an error code returned by this function.
*
*                                OS_ERR_NONE                    if the call was successful
*                                OS_ERR_CREATE_ISR              if you called this function from an ISR
*                                OS_ERR_ILLEGAL_CREATE_RUN_TIME if you are trying to create the Mutex after you called
*                                                                 OSSafetyCriticalStart().
*                                OS_ERR_NAME                    if 'p_name'  is a NULL pointer
*                                OS_ERR_OBJ_CREATED             if the mutex has already been created
*                                OS_ERR_OBJ_PTR_NULL            if 'p_mutex' is a NULL pointer
*
* Returns    : none
************************************************************************************************************************
*/

void  OSMutexCreate (OS_MUTEX    *p_mutex,
                     CPU_CHAR    *p_name,
                     OS_ERR      *p_err)
{
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to be called from an ISR                   */
        *p_err = OS_ERR_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    p_mutex->Type              =  OS_OBJ_TYPE_MUTEX;        /* Mark the data structure as a mutex                     */
    p_mutex->NamePtr           =  p_name;
    p_mutex->OwnerTCBPtr       = (OS_TCB       *)0;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)0;         /* Mutex is available                                     */
    p_mutex->TS                = (CPU_TS        )0;
    p_mutex->OwnerOriginalPrio =  OS_CFG_PRIO_MAX;
    OS_PendListInit(&p_mutex->PendList);                    /* Initialize the waiting list                            */

    /*User to added, thing to do here
    -> Splay tree data structure implementation
    -> Another Mutex node creation for Splay tree data structure., Mutex_node creation done in the beginnning of this code
    -> Splay tree Mutex node is store all of the p_mutex node data.
    -> Insert in to the splay tree
    */

    if (init_splay_tree == 0) {
		splay_nil_root.l=NULL;
		splay_nil_root.r=NULL;
		splay_nil_root.p=NULL;
                splay_nil_root.v=0;   
                splay_nil_root.mutex_id = 0;
                splay_nil_root.task_preemp_lvl=0;
                splay_nil_root.OS_TCB_MUTEX_OWNER = NULL;
                splay_root = &splay_nil_root;
                splay_preemp_root = &splay_nil_root;
                init_splay_tree = 1;
    }
    
    mutex_character = *(p_name);
    
    if (mutex_character == 'a'){
      //assign node infor
      mutex1_node.l = NULL;
      mutex1_node.r = NULL;
      mutex1_node.p = NULL;  
      mutex1_node.OS_TCB_MUTEX_OWNER = NULL;
      mutex1_node.v = 1;//mutex identification
      mutex1_node.mutex_id = 1;
      mutex1_node.task_preemp_lvl = 0;
      splay_Insert(1,&mutex1_node);   

      mutex1_preemp_node.l = NULL;
      mutex1_preemp_node.r = NULL;
      mutex1_preemp_node.p = NULL;  
      mutex1_preemp_node.OS_TCB_MUTEX_OWNER = NULL;
      mutex1_preemp_node.v = 0;//preemption lvl
      mutex1_preemp_node.mutex_id = 1;
      mutex1_preemp_node.task_preemp_lvl = 0;
    }
    
    if (mutex_character == 'b'){
      //assign node infor
      mutex2_node.l = NULL;
      mutex2_node.r = NULL;
      mutex2_node.p = NULL; 
      mutex2_node.OS_TCB_MUTEX_OWNER = NULL;
      mutex2_node.v = 2;//mutex identification
      mutex2_node.mutex_id = 2;
      mutex2_node.task_preemp_lvl = 0;     
      splay_Insert(2,&mutex2_node); 
      
      mutex2_preemp_node.l = NULL;
      mutex2_preemp_node.r = NULL;
      mutex2_preemp_node.p = NULL;  
      mutex2_preemp_node.OS_TCB_MUTEX_OWNER = NULL;
      mutex2_preemp_node.v = 0;//preemption lvl
      mutex2_preemp_node.mutex_id = 2;
      mutex2_preemp_node.task_preemp_lvl = 0;  //same as v
    }
    
    if (mutex_character == 'c'){
      //assign node infor
      mutex3_node.l = NULL;
      mutex3_node.r = NULL;
      mutex3_node.p = NULL; 
      mutex3_node.OS_TCB_MUTEX_OWNER = NULL;
      mutex3_node.v = 3;//mutex identification
      mutex3_node.mutex_id = 3;//mutex identification
      mutex3_node.task_preemp_lvl = 0;      
      splay_Insert(3,&mutex3_node);

      mutex3_preemp_node.l = NULL;
      mutex3_preemp_node.r = NULL;
      mutex3_preemp_node.p = NULL;  
      mutex3_preemp_node.OS_TCB_MUTEX_OWNER = NULL;
      mutex3_preemp_node.v = 0;//preemption lvl
      mutex3_preemp_node.mutex_id = 3;
      mutex3_preemp_node.task_preemp_lvl = 0;  //same as v 
      
    }
      
    
#if OS_CFG_DBG_EN > 0u
    OS_MutexDbgListAdd(p_mutex);
#endif
    OSMutexQty++;

    CPU_CRITICAL_EXIT();
    *p_err = OS_ERR_NONE;
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                                   DELETE A MUTEX
*
* Description: This function deletes a mutex and readies all tasks pending on the mutex.
*
* Arguments  : p_mutex       is a pointer to the mutex to delete
*
*              opt           determines delete options as follows:
*
*                                OS_OPT_DEL_NO_PEND          Delete mutex ONLY if no task pending
*                                OS_OPT_DEL_ALWAYS           Deletes the mutex even if tasks are waiting.
*                                                            In this case, all the tasks pending will be readied.
*
*              p_err         is a pointer to a variable that will contain an error code returned by this function.
*
*                                OS_ERR_NONE                 The call was successful and the mutex was deleted
*                                OS_ERR_DEL_ISR              If you attempted to delete the mutex from an ISR
*                                OS_ERR_OBJ_PTR_NULL         If 'p_mutex' is a NULL pointer.
*                                OS_ERR_OBJ_TYPE             If 'p_mutex' is not pointing to a mutex
*                                OS_ERR_OPT_INVALID          An invalid option was specified
*                                OS_ERR_STATE_INVALID        Task is in an invalid state
*                                OS_ERR_TASK_WAITING         One or more tasks were waiting on the mutex
*
* Returns    : == 0          if no tasks were waiting on the mutex, or upon error.
*              >  0          if one or more tasks waiting on the mutex are now readied and informed.
*
* Note(s)    : 1) This function must be used with care.  Tasks that would normally expect the presence of the mutex MUST
*                 check the return code of OSMutexPend().
*
*              2) OSMutexAccept() callers will not know that the intended mutex has been deleted.
*
*              3) Because ALL tasks pending on the mutex will be readied, you MUST be careful in applications where the
*                 mutex is used for mutual exclusion because the resource(s) will no longer be guarded by the mutex.
************************************************************************************************************************
*/

#if OS_CFG_MUTEX_DEL_EN > 0u
OS_OBJ_QTY  OSMutexDel (OS_MUTEX  *p_mutex,
                        OS_OPT     opt,
                        OS_ERR    *p_err)
{
    OS_OBJ_QTY     cnt;
    OS_OBJ_QTY     nbr_tasks;
    OS_PEND_DATA  *p_pend_data;
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    OS_TCB        *p_tcb_owner;
    CPU_TS         ts;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return ((OS_OBJ_QTY)0);
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {                   /* Not allowed to delete a mutex from an ISR         */
       *p_err = OS_ERR_DEL_ISR;
        return ((OS_OBJ_QTY)0);
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                              /* Validate pointer to mutex                         */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return ((OS_OBJ_QTY)0);
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {                    /* Make sure mutex was created                       */
        *p_err = OS_ERR_OBJ_TYPE;
        return ((OS_OBJ_QTY)0);
    }
#endif

    OS_CRITICAL_ENTER();
    p_pend_list = &p_mutex->PendList;
    cnt         = p_pend_list->NbrEntries;
    nbr_tasks   = cnt;
    switch (opt) {
        case OS_OPT_DEL_NO_PEND:                                 /* Delete mutex only if no task waiting              */
             if (nbr_tasks == (OS_OBJ_QTY)0) {
#if OS_CFG_DBG_EN > 0u
                 OS_MutexDbgListRemove(p_mutex);
#endif
                 OSMutexQty--;
                 OS_MutexClr(p_mutex);
                 OS_CRITICAL_EXIT();
                 *p_err = OS_ERR_NONE;
             } else {
                 OS_CRITICAL_EXIT();
                 *p_err = OS_ERR_TASK_WAITING;
             }
             break;

        case OS_OPT_DEL_ALWAYS:                                            /* Always delete the mutex                 */
             p_tcb_owner = p_mutex->OwnerTCBPtr;                           /* Did we had to change the prio of owner? */
             if ((p_tcb_owner       != (OS_TCB *)0) &&
                 (p_tcb_owner->Prio !=  p_mutex->OwnerOriginalPrio)) {
                 switch (p_tcb_owner->TaskState) {                         /* yes                                     */
                     case OS_TASK_STATE_RDY:
                          OS_RdyListRemove(p_tcb_owner);
                          p_tcb_owner->Prio = p_mutex->OwnerOriginalPrio;  /* Lower owner's prio back                 */
                          OS_PrioInsert(p_tcb_owner->Prio);
                          OS_RdyListInsertTail(p_tcb_owner);               /* Insert owner in ready list at new prio  */
                          break;

                     case OS_TASK_STATE_DLY:
                     case OS_TASK_STATE_SUSPENDED:
                     case OS_TASK_STATE_DLY_SUSPENDED:
                          p_tcb_owner->Prio = p_mutex->OwnerOriginalPrio;  /* Not in any pend list, change the prio   */
                          break;

                     case OS_TASK_STATE_PEND:
                     case OS_TASK_STATE_PEND_TIMEOUT:
                     case OS_TASK_STATE_PEND_SUSPENDED:
                     case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
                          OS_PendListChangePrio(p_tcb_owner,               /* Owner is pending on another object      */
                                                p_mutex->OwnerOriginalPrio);
                          break;

                     default:
                          OS_CRITICAL_EXIT();
                          *p_err = OS_ERR_STATE_INVALID;
                          return ((OS_OBJ_QTY)0);
                 }
             }

             ts = OS_TS_GET();                                             /* Get timestamp                           */
             while (cnt > 0u) {                                            /* Remove all tasks from the pend list     */
                 p_pend_data = p_pend_list->HeadPtr;
                 p_tcb       = p_pend_data->TCBPtr;
                 OS_PendObjDel((OS_PEND_OBJ *)((void *)p_mutex),
                               p_tcb,
                               ts);
                 cnt--;
             }
#if OS_CFG_DBG_EN > 0u
             OS_MutexDbgListRemove(p_mutex);
#endif
             OSMutexQty--;
             OS_MutexClr(p_mutex);
             OS_CRITICAL_EXIT_NO_SCHED();
             OSSched();                                                    /* Find highest priority task ready to run */
             *p_err = OS_ERR_NONE;
             break;

        default:
             OS_CRITICAL_EXIT();
             *p_err = OS_ERR_OPT_INVALID;
             break;
    }
    return (nbr_tasks);
}
#endif

/*$PAGE*/
/*
************************************************************************************************************************
*                                                    PEND ON MUTEX
*
* Description: This function waits for a mutex.
*
* Arguments  : p_mutex       is a pointer to the mutex
*
*              timeout       is an optional timeout period (in clock ticks).  If non-zero, your task will wait for the
*                            resource up to the amount of time (in 'ticks') specified by this argument.  If you specify
*                            0, however, your task will wait forever at the specified mutex or, until the resource
*                            becomes available.
*
*              opt           determines whether the user wants to block if the mutex is not available or not:
*
*                                OS_OPT_PEND_BLOCKING
*                                OS_OPT_PEND_NON_BLOCKING
*
*              p_ts          is a pointer to a variable that will receive the timestamp of when the mutex was posted or
*                            pend aborted or the mutex deleted.  If you pass a NULL pointer (i.e. (CPU_TS *)0) then you
*                            will not get the timestamp.  In other words, passing a NULL pointer is valid and indicates
*                            that you don't need the timestamp.
*
*              p_err         is a pointer to a variable that will contain an error code returned by this function.
*
*                                OS_ERR_NONE               The call was successful and your task owns the resource
*                                OS_ERR_MUTEX_OWNER        If calling task already owns the mutex
*                                OS_ERR_OBJ_DEL            If 'p_mutex' was deleted
*                                OS_ERR_OBJ_PTR_NULL       If 'p_mutex' is a NULL pointer.
*                                OS_ERR_OBJ_TYPE           If 'p_mutex' is not pointing at a mutex
*                                OS_ERR_OPT_INVALID        If you didn't specify a valid option
*                                OS_ERR_PEND_ABORT         If the pend was aborted by another task
*                                OS_ERR_PEND_ISR           If you called this function from an ISR and the result
*                                                          would lead to a suspension.
*                                OS_ERR_PEND_WOULD_BLOCK   If you specified non-blocking but the mutex was not
*                                                          available.
*                                OS_ERR_SCHED_LOCKED       If you called this function when the scheduler is locked
*                                OS_ERR_STATE_INVALID      If the task is in an invalid state
*                                OS_ERR_STATUS_INVALID     If the pend status has an invalid value
*                                OS_ERR_TIMEOUT            The mutex was not received within the specified timeout.
*
* Returns    : none
************************************************************************************************************************
*/

void  OSMutexPend (OS_MUTEX   *p_mutex,
                   OS_TICK     timeout,
                   OS_OPT      opt,
                   CPU_TS     *p_ts,
                   OS_ERR     *p_err)
{
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();
    struct mutex_node* mutex_ptr;
    int val;
    /*
    CPU_TS Mutex_StartTime;
    CPU_TS Mutex_StartTime2;        
    CPU_TS Mutex_OverheadValue;   
    CPU_TS Mutex_OverheadValue_tmp;
*/
    /*
    CPU_TS_TMR Mutex_StartTime;
    CPU_TS_TMR Mutex_StartTime2;        
    CPU_TS_TMR Mutex_OverheadValue;   
    CPU_TS_TMR Mutex_OverheadValue_tmp;
    */
#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_PEND_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate arguments                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
    switch (opt) {
        case OS_OPT_PEND_BLOCKING:
        case OS_OPT_PEND_NON_BLOCKING:
             break;

        default:
             *p_err = OS_ERR_OPT_INVALID;
             return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    if (p_ts != (CPU_TS *)0) {
       *p_ts  = (CPU_TS  )0;                                /* Initialize the returned timestamp                      */
    } 
    
    CPU_CRITICAL_ENTER();
    
    //User Added, determine the preemption level of the OCB CURRENT TASK
    if (*OSTCBCurPtr->NamePtr == '1')
      TASK_PREEMP = 7;
    else if (*OSTCBCurPtr->NamePtr == '2')
      TASK_PREEMP = 6;
    else if (*OSTCBCurPtr->NamePtr == '3')
      TASK_PREEMP = 5;     
 
    //User added
    //Check the preemption level of the TASK is higher than the system ceiling
    //Or the current task TCB is the owner of the previous mutex owner
    if (TASK_PREEMP > sys_ceiling || OSTCBCurPtr  == OS_TCB_MUTEX_PRE_OWNER){
    if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0) {    /* Resource available?                                    */
        p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
        p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
        p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
        if (p_ts != (CPU_TS *)0) {
           *p_ts                   = p_mutex->TS;
        }

    //User added,Finding the mutex node
    //*p_mutex->NamePtr = a == mutexone, b == mutextwo, c == mutexthree, 
    if (*p_mutex->NamePtr == 'a'){
      mutex_ptr = Find(1);
      //Insert into the preemp Splay tree data structure
      mutex1_preemp_node.OS_TCB_MUTEX_OWNER = OSTCBCurPtr;
      splay_preemp_Insert(TASK_PREEMP,&mutex1_preemp_node);
      
    } else if (*p_mutex->NamePtr == 'b') { 
      mutex_ptr = Find(2);
      mutex2_preemp_node.OS_TCB_MUTEX_OWNER = OSTCBCurPtr;
      splay_preemp_Insert(TASK_PREEMP,&mutex2_preemp_node); 

    } else if (*p_mutex->NamePtr == 'c') {
      mutex_ptr = Find(3);
      mutex3_preemp_node.OS_TCB_MUTEX_OWNER = OSTCBCurPtr;
      splay_preemp_Insert(TASK_PREEMP,&mutex3_preemp_node); 
    }
        /*User Added, To be done, Things to do here
        -> update the resource ceiling
        -> Update the mutex owner
        */
        if (mutex_ptr->v == 1) //Mutex1 resource
          sys_ceiling = 5;
        else if (mutex_ptr->v== 2) //Mutex2 resource
          sys_ceiling = 7;     
        else if (mutex_ptr->v == 3) //Mutex3 resource
          sys_ceiling = 7;    
        //-> Update the mutex pre owner
        //-> Update the mutex owner for the mutex node
        if (mutex_pre_own == 0) {
        OS_TCB_MUTEX_PRE_OWNER = OSTCBCurPtr;
        MUTEX_PRE_OWN = mutex_ptr;
        mutex_pre_own = 1;
        }
        mutex_ptr->OS_TCB_MUTEX_OWNER = OSTCBCurPtr;
        mutex_ptr->task_preemp_lvl = TASK_PREEMP;
         
        CPU_CRITICAL_EXIT();
        *p_err                     =  OS_ERR_NONE;
        return;
    }
    }
    
    /*User Added, To be done, Things to do here
    -> Perform a Splay Tree Mutex node search from the current given passing p_mutex.
    -> Check the Priority(preemption level) of the Current Task(OS_TASKTCB) is higher than the System Ceiling 
    ->If yes, Mutex resource granted to the current Task
        -> Mutex resource granted to the current Task, update p_mutex node.
        -> Update system ceiling => a new data in the mutex node. or probably a global variable
        -> Update the Mutex node to the current Task, update p_mutex node to allign with the OS_TASKTCB
        -> Update the Splay Tree Mutex node also
    ->If no
        -> Add the Current Task into the Block list
    -> Block list node data struct , 2-3-4 data structure implementation:
        -> Task node pointer OS_TASK_TCB
        -> Store the Mutex name in to the BLock list node also(new)
    */
    
    if (OSTCBCurPtr == p_mutex->OwnerTCBPtr) {              /* See if current task is already the owner of the mutex  */
        p_mutex->OwnerNestingCtr++;
        if (p_ts != (CPU_TS *)0) {
           *p_ts  = p_mutex->TS;
        }
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_OWNER;                        /* Indicate that current task already owns the mutex      */
        return;
    }

    if ((opt & OS_OPT_PEND_NON_BLOCKING) != (OS_OPT)0) {    /* Caller wants to block if not available?                */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_PEND_WOULD_BLOCK;                   /* No                                                     */
        return;
    } else {
        if (OSSchedLockNestingCtr > (OS_NESTING_CTR)0) {    /* Can't pend when the scheduler is locked                */
            CPU_CRITICAL_EXIT();
            *p_err = OS_ERR_SCHED_LOCKED;
            return;
        }
    }

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();                  /* Lock the scheduler/re-enable interrupts                */
    p_tcb = p_mutex->OwnerTCBPtr;                           /* Point to the TCB of the Mutex owner                    */
    if (p_tcb->Prio > OSTCBCurPtr->Prio) {                  /* See if mutex owner has a lower priority than current   */
        switch (p_tcb->TaskState) {
            case OS_TASK_STATE_RDY:                 
                 OS_RdyListRemove(p_tcb);                   /* Remove from ready list at current priority             */
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Raise owner's priority                                 */
                 OS_PrioInsert(p_tcb->Prio);
                 OS_RdyListInsertHead(p_tcb);               /* Insert in ready list at new priority                   */
                 break;

            case OS_TASK_STATE_DLY:
            case OS_TASK_STATE_DLY_SUSPENDED:
            case OS_TASK_STATE_SUSPENDED:
                 p_tcb->Prio = OSTCBCurPtr->Prio;           /* Only need to raise the owner's priority                */
                 break;

            case OS_TASK_STATE_PEND:                        /* Change the position of the task in the wait list       */
            case OS_TASK_STATE_PEND_TIMEOUT:
            case OS_TASK_STATE_PEND_SUSPENDED:
            case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
                 OS_PendListChangePrio(p_tcb,
                                       OSTCBCurPtr->Prio);
                 break;

            default:
                 OS_CRITICAL_EXIT();
                 *p_err = OS_ERR_STATE_INVALID;
                 return;
        }
    }

    //User added, update Block list
    //Insert into the block list, 234 tree data structure,val = preemption lvl
    val = TASK_PREEMP;
    insertion(val,OSTCBCurPtr,p_mutex,&mutexX_node_block);
    
    if (BENCHMARK_CODE > 0) {
        if (*p_mutex->NamePtr == 'a')
              mutex_ptr = Find(1);
        if (*p_mutex->NamePtr == 'b')
              mutex_ptr = Find(2);
        if (*p_mutex->NamePtr == 'c')
              mutex_ptr = Find(3);
        
      if (MUTEX_PRE_OWN->task_preemp_lvl < TASK_PREEMP) { //if the mutex owner preemption lvl is lower than the current task preemption lvl
      Mutex_StartTime = OS_TS_GET();
      Mutex_StartTime2 = OS_TS_GET();          
      }
    }
    
    OS_Pend(&pend_data,                                     /* Block task pending on Mutex                            */
            (OS_PEND_OBJ *)((void *)p_mutex),
             OS_TASK_PEND_ON_MUTEX,
             timeout);
    
    OS_CRITICAL_EXIT_NO_SCHED();

    OSSched();                                              /* Find the next highest priority task ready to run       */

    CPU_CRITICAL_ENTER();
    
    //User added, determine and change the MUTEX_PRE_OWNER once we got the mutex

     if (BENCHMARK_CODE > 0) { 
      //if (MUTEX_PRE_OWN->task_preemp_lvl < TASK_PREEMP) {
        Mutex_OverheadValue_tmp = OS_TS_GET();
        Mutex_OverheadValue = Mutex_StartTime2 + Mutex_OverheadValue_tmp - Mutex_StartTime2 - Mutex_StartTime;
      //}
        Mutex_OverheadValue_tmp = Mutex_OverheadValue;
    }
    
    
    switch (OSTCBCurPtr->PendStatus) {
        case OS_STATUS_PEND_OK:                             /* We got the mutex                                       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_NONE;
             break;

        case OS_STATUS_PEND_ABORT:                          /* Indicate that we aborted                               */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_PEND_ABORT;
             break;

        case OS_STATUS_PEND_TIMEOUT:                        /* Indicate that we didn't get mutex within timeout       */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = (CPU_TS  )0;
             }
             *p_err = OS_ERR_TIMEOUT;
             break;

        case OS_STATUS_PEND_DEL:                            /* Indicate that object pended on has been deleted        */
             if (p_ts != (CPU_TS *)0) {
                *p_ts  = OSTCBCurPtr->TS;
             }
             *p_err = OS_ERR_OBJ_DEL;
             break;

        default:
             *p_err = OS_ERR_STATUS_INVALID;
             break;
    }
    CPU_CRITICAL_EXIT();
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                               ABORT WAITING ON A MUTEX
*
* Description: This function aborts & readies any tasks currently waiting on a mutex.  This function should be used
*              to fault-abort the wait on the mutex, rather than to normally signal the mutex via OSMutexPost().
*
* Arguments  : p_mutex   is a pointer to the mutex
*
*              opt       determines the type of ABORT performed:
*
*                            OS_OPT_PEND_ABORT_1          ABORT wait for a single task (HPT) waiting on the mutex
*                            OS_OPT_PEND_ABORT_ALL        ABORT wait for ALL tasks that are  waiting on the mutex
*                            OS_OPT_POST_NO_SCHED         Do not call the scheduler
*
*              p_err     is a pointer to a variable that will contain an error code returned by this function.
*
*                            OS_ERR_NONE                  At least one task waiting on the mutex was readied and
*                                                         informed of the aborted wait; check return value for the
*                                                         number of tasks whose wait on the mutex was aborted.
*                            OS_ERR_OBJ_PTR_NULL          If 'p_mutex' is a NULL pointer.
*                            OS_ERR_OBJ_TYPE              If 'p_mutex' is not pointing at a mutex
*                            OS_ERR_OPT_INVALID           If you specified an invalid option
*                            OS_ERR_PEND_ABORT_ISR        If you attempted to call this function from an ISR
*                            OS_ERR_PEND_ABORT_NONE       No task were pending
*
* Returns    : == 0          if no tasks were waiting on the mutex, or upon error.
*              >  0          if one or more tasks waiting on the mutex are now readied and informed.
************************************************************************************************************************
*/

#if OS_CFG_MUTEX_PEND_ABORT_EN > 0u
OS_OBJ_QTY  OSMutexPendAbort (OS_MUTEX  *p_mutex,
                              OS_OPT     opt,
                              OS_ERR    *p_err)
{
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    CPU_TS         ts;
    OS_OBJ_QTY     nbr_tasks;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return ((OS_OBJ_QTY)0u);
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0u) {             /* Not allowed to Pend Abort from an ISR                  */
       *p_err =  OS_ERR_PEND_ABORT_ISR;
        return ((OS_OBJ_QTY)0u);
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
       *p_err =  OS_ERR_OBJ_PTR_NULL;
        return ((OS_OBJ_QTY)0u);
    }
    switch (opt) {                                          /* Validate 'opt'                                         */
        case OS_OPT_PEND_ABORT_1:
        case OS_OPT_PEND_ABORT_ALL:
             break;

        default:
            *p_err =  OS_ERR_OPT_INVALID;
             return ((OS_OBJ_QTY)0u);
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
       *p_err =  OS_ERR_OBJ_TYPE;
        return ((OS_OBJ_QTY)0u);
    }
#endif

    CPU_CRITICAL_ENTER();
    p_pend_list = &p_mutex->PendList;
    if (p_pend_list->NbrEntries == (OS_OBJ_QTY)0u) {        /* Any task waiting on mutex?                             */
        CPU_CRITICAL_EXIT();                                /* No                                                     */
       *p_err =  OS_ERR_PEND_ABORT_NONE;
        return ((OS_OBJ_QTY)0u);
    }

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();
    nbr_tasks = 0u;
    ts        = OS_TS_GET();                                /* Get local time stamp so all tasks get the same time    */
    while (p_pend_list->NbrEntries > (OS_OBJ_QTY)0u) {
        p_tcb = p_pend_list->HeadPtr->TCBPtr;
        OS_PendAbort((OS_PEND_OBJ *)((void *)p_mutex),
                     p_tcb,
                     ts);
        nbr_tasks++;
        if (opt != OS_OPT_PEND_ABORT_ALL) {                 /* Pend abort all tasks waiting?                          */
            break;                                          /* No                                                     */
        }
    }
    OS_CRITICAL_EXIT_NO_SCHED();

    if ((opt & OS_OPT_POST_NO_SCHED) == (OS_OPT)0u) {
        OSSched();                                          /* Run the scheduler                                      */
    }

   *p_err = OS_ERR_NONE;
    return (nbr_tasks);
}
#endif

/*$PAGE*/
/*
************************************************************************************************************************
*                                                   POST TO A MUTEX
*
* Description: This function signals a mutex
*
* Arguments  : p_mutex  is a pointer to the mutex
*
*              opt      is an option you can specify to alter the behavior of the post.  The choices are:
*
*                           OS_OPT_POST_NONE        No special option selected
*                           OS_OPT_POST_NO_SCHED    If you don't want the scheduler to be called after the post.
*
*              p_err    is a pointer to a variable that will contain an error code returned by this function.
*
*                           OS_ERR_NONE             The call was successful and the mutex was signaled.
*                           OS_ERR_MUTEX_NESTING    Mutex owner nested its use of the mutex
*                           OS_ERR_MUTEX_NOT_OWNER  If the task posting is not the Mutex owner
*                           OS_ERR_OBJ_PTR_NULL     If 'p_mutex' is a NULL pointer.
*                           OS_ERR_OBJ_TYPE         If 'p_mutex' is not pointing at a mutex
*                           OS_ERR_POST_ISR         If you attempted to post from an ISR
*
* Returns    : none
************************************************************************************************************************
*/

void  OSMutexPost (OS_MUTEX  *p_mutex,
                   OS_OPT     opt,
                   OS_ERR    *p_err)
{
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    CPU_TS         ts;
    CPU_SR_ALLOC();
    struct mutex_node* mutex_post_ptr;
    struct mutex_node* mutex_post_nxt_high_ptr;
    struct mutex_node* mutex_post_preemp_ptr;
    int task_preempt_lvl_value;
    int any_task_mutex_owner;
    int OSTCB_TASK_PREEMP;
    int OTHER_MUTEX_BLOCK;
    OS_TCB* OS_TCB_MUTEX_PRE_OWNER_TMP;
    struct mutex_node* search_tcb_owner_other_mutex;
    int TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX;
    //int block_list_count;
    int count_tmp;
    //struct btreeNode* max_preemp_block;    
    OS_TCB* max_preemp_block;
    
#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_POST_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    if (OSTCBCurPtr != p_mutex->OwnerTCBPtr) {              /* Make sure the mutex owner is releasing the mutex       */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_NOT_OWNER;
        return;
    }

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();
    ts          = OS_TS_GET();                              /* Get timestamp                                          */
    p_mutex->TS = ts;
    p_mutex->OwnerNestingCtr--;                             /* Decrement owner's nesting counter                      */
    if (p_mutex->OwnerNestingCtr > (OS_NESTING_CTR)0) {     /* Are we done with all nestings?                         */
        OS_CRITICAL_EXIT();                                 /* No                                                     */
        *p_err = OS_ERR_MUTEX_NESTING;
        return;
    }

    //User Added, Splay tree search
    //-> Splay tree is updated also
    //->*p_mutex->NamePtr = a == AppTaskOne, b == AppTaskTwo, c == AppTaskThree,
    if (*p_mutex->NamePtr == 'a'){
      mutex_post_ptr = Find(1);
      task_preempt_lvl_value = mutex_post_ptr->task_preemp_lvl;
      mutex_post_preemp_ptr = Find_preemp(task_preempt_lvl_value);
      //If  the mutex id is not match,because the it is possible that the OCBTASK is own for 2 mutex
      //It will be in the left of the first finding,keep searching for the left node
      while(mutex_post_preemp_ptr->mutex_id!=1){mutex_post_preemp_ptr = mutex_post_preemp_ptr->l;};

      //Delete the splay_preemp tree node with allocated task_preemption_lvl value
      Erase_preemp(task_preempt_lvl_value,1);
      mutex_post_preemp_ptr->l = NULL;
      mutex_post_preemp_ptr->r = NULL;
      mutex_post_preemp_ptr->p = NULL;
      mutex_post_preemp_ptr->OS_TCB_MUTEX_OWNER = NULL;
      mutex_post_preemp_ptr->v = 0;
      mutex_post_preemp_ptr->task_preemp_lvl = 0;    
    
    } else if (*p_mutex->NamePtr == 'b') {
      mutex_post_ptr = Find(2);
      task_preempt_lvl_value = mutex_post_ptr->task_preemp_lvl;
      mutex_post_preemp_ptr = Find_preemp(task_preempt_lvl_value);
      //If  the mutex id is not match,because the it is possible that the OCBTASK is own for 2 mutex
      //It will be in the left of the first finding,keep searching for the left node
      while(mutex_post_preemp_ptr->mutex_id!=2){mutex_post_preemp_ptr = mutex_post_preemp_ptr->l;};

      //Delete the splay_preemp tree node with allocated task_preemption_lvl value
      Erase_preemp(task_preempt_lvl_value,2);   
      mutex_post_preemp_ptr->l = NULL;
      mutex_post_preemp_ptr->r = NULL;
      mutex_post_preemp_ptr->p = NULL;
      mutex_post_preemp_ptr->OS_TCB_MUTEX_OWNER = NULL;
      mutex_post_preemp_ptr->v = 0;
      mutex_post_preemp_ptr->task_preemp_lvl = 0;         
    } else if (*p_mutex->NamePtr == 'c') {
      mutex_post_ptr = Find(3); 
      task_preempt_lvl_value = mutex_post_ptr->task_preemp_lvl;
      mutex_post_preemp_ptr = Find_preemp(task_preempt_lvl_value);
      //If  the mutex id is not match,because the it is possible that the OCBTASK is own for 2 mutex
      //It will be in the left of the first finding,keep searching for the left node
      while(mutex_post_preemp_ptr->mutex_id!=3){mutex_post_preemp_ptr = mutex_post_preemp_ptr->l;};

      //Delete the splay_preemp tree node
      Erase_preemp(task_preempt_lvl_value,3);  
      mutex_post_preemp_ptr->l = NULL;
      mutex_post_preemp_ptr->r = NULL;
      mutex_post_preemp_ptr->p = NULL;
      mutex_post_preemp_ptr->OS_TCB_MUTEX_OWNER = NULL;
      mutex_post_preemp_ptr->v = 0;
      mutex_post_preemp_ptr->task_preemp_lvl = 0;   
    }

    OTHER_MUTEX_BLOCK = 0;
    p_pend_list = &p_mutex->PendList;
    if (p_pend_list->NbrEntries == (OS_OBJ_QTY)0) {         /* Any task waiting on mutex?                             */
        p_mutex->OwnerTCBPtr     = (OS_TCB       *)0;       /* No                                                     */
        p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0;

        //Temporary store the OS_TCB_MUTEX_OWNER in the adjusting system ceiling block
        OS_TCB_MUTEX_PRE_OWNER_TMP = OS_TCB_MUTEX_PRE_OWNER;
        
    //Check if the OCTCBCURTASK that release the mutex is == OSTCBMUTEXPRE_OWNER
    //To check and if there is an update to release the OS_TCB_MUTEX_PRE_OWNER(last mutex owner)
    if ((OSTCBCurPtr == mutex_post_ptr->OS_TCB_MUTEX_OWNER) && (OSTCBCurPtr == OS_TCB_MUTEX_PRE_OWNER) && (MUTEX_PRE_OWN == mutex_post_ptr)) {
      OS_TCB_MUTEX_PRE_OWNER = NULL;            //Release OS_TCB_MUTEX_PRE_OWNER to NULL
      MUTEX_PRE_OWN          = NULL;
      mutex_pre_own = 0;                        //Clear mutex_pre_own 
    }        
        
        //clear the mutex node OCT_TASK_OWNER
        mutex_post_ptr->OS_TCB_MUTEX_OWNER = NULL;
        mutex_post_ptr->task_preemp_lvl = 0; 
        
        //User added
        //Check if there is any Block node list
        //if(OS_TCB_BLOCK1 == NULL && OS_TCB_BLOCK2 == NULL && OS_TCB_BLOCK3 == NULL)
        if(root_btree == NULL){
          OS_CRITICAL_EXIT();
         *p_err = OS_ERR_NONE;
         
         //searching for all of the mutex node if there is any occupying of any other
         //mutex node for the OS_TCB_MUTEX_PRE_OWNER
         //Keep searching to the right
         //Search only if OS_TCB_MUTEX_PRE_OWNER_TMP != NULL
         TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX = 0;
         if (OS_TCB_MUTEX_PRE_OWNER_TMP != NULL) {
         search_tcb_owner_other_mutex = splay_root;
         while (search_tcb_owner_other_mutex != NULL && TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX == 0){
           if(search_tcb_owner_other_mutex->OS_TCB_MUTEX_OWNER == OS_TCB_MUTEX_PRE_OWNER_TMP)
               TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX = 1;
           else
             //searching to the right  
             search_tcb_owner_other_mutex = search_tcb_owner_other_mutex->r;   
         }

         //Keep searching to the left
         if (TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX == 0){
         search_tcb_owner_other_mutex = splay_root;
         while (search_tcb_owner_other_mutex != NULL && TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX == 0){
           if(search_tcb_owner_other_mutex->OS_TCB_MUTEX_OWNER == OS_TCB_MUTEX_PRE_OWNER_TMP)
               TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX = 1;
           else
             //searching to the left
             search_tcb_owner_other_mutex = search_tcb_owner_other_mutex->l;   
         }
         }
         }
         
         //No more block task for any mutex and the OCST_TCB_PREONWER had release all the mutex, adjust system ceiling to the lowest
         if (TCB_MUTEX_PREOWNER_OWN_OTHER_MUTEX == 0)
            sys_ceiling = 4;
         
          return; //Return only if there is no OSTCB_BLOCK list for all of the mutex
        }else
          OTHER_MUTEX_BLOCK = 1;//Indicate there is other blocking task that waiting for another mutex
    }   
    
                                                                /* Yes */
    /* User to Be done added
    The mutex resource node is release, things to do is :
    -> Perform a Splay Tree Mutex search given p_mutex
    -> Check and assign NULL to OS_TCB_MUTEX_PRE_OWNER if the first OCB TASK owner release the very first mutex
    -> Adjust the system ceiling(preemption level to the lowest) among the 3 task node
    -> First check from the block list is it a NULL node?
        -> if it is a NULL,assign the mutex node of TASKTCB to NULL
        -> it it is not a NULL, mean there is a task added into the block list, perform a search for highest
           priority job,
        -> Once the highest priority job found in the block list, search for the Mutex name and post it.
    
    */
    //User added,
    //First check if OTHER_MUTEX_BLOCK = 0,
    if (OTHER_MUTEX_BLOCK == 0){//It is 0, means there is pending TASK on the  same MUTEX
    //User added
    //To check and if there is an update to release the OS_TCB_MUTEX_PRE_OWNER(last mutex owner)
    if ((OSTCBCurPtr == mutex_post_ptr->OS_TCB_MUTEX_OWNER) && (OSTCBCurPtr == OS_TCB_MUTEX_PRE_OWNER) && (MUTEX_PRE_OWN == mutex_post_ptr)) {
      OS_TCB_MUTEX_PRE_OWNER = NULL;            //Release OS_TCB_MUTEX_PRE_OWNER to NULL
      mutex_pre_own = 0;                        //Clear mutex_pre_own 
    }

    //User added
    //Assign NULL to OS_TCB_MUTEX_OWNER of the mutex node
    //Assign 0 to task_preemp_lvl
    mutex_post_ptr->OS_TCB_MUTEX_OWNER = NULL;
    mutex_post_ptr->task_preemp_lvl = 0;
      
    //User added
    //->Check for all of the Mutex node, if there is any OSTCBTASK owner or not
    //  -> If yes, find the next highest peremp,and adjust the system ceiling accordingly
    //  -> if Not, no need to find, adjust the system ceiling to the lowest value
    any_task_mutex_owner = 0;
    any_task_mutex_owner = Inorder(splay_root);
    if (any_task_mutex_owner == 1) {//Find the next highest task preemp level and adjust system ceiling base on which mutex
      mutex_post_nxt_high_ptr = splay_preemp_max(splay_preemp_root);
      if(mutex_post_nxt_high_ptr->mutex_id == 1) 
        sys_ceiling = 5;
      else if (mutex_post_nxt_high_ptr->mutex_id == 2) 
        sys_ceiling = 7;
      else if (mutex_post_nxt_high_ptr->mutex_id == 3) 
        sys_ceiling = 7;        
    } else {    //All the mutex are free of OSTCBTASK owner
      sys_ceiling = 4;
    }
                                   
    if (OSTCBCurPtr->Prio != p_mutex->OwnerOriginalPrio) {
        OS_RdyListRemove(OSTCBCurPtr);
        OSTCBCurPtr->Prio = p_mutex->OwnerOriginalPrio;     /* Lower owner's priority back to its original one        */
        OS_PrioInsert(OSTCBCurPtr->Prio);
        OS_RdyListInsertTail(OSTCBCurPtr);                  /* Insert owner in ready list at new priority             */
        OSPrioCur         = OSTCBCurPtr->Prio;
    }
    
    //User Added, assign the OSTASKTCB that is in the block list with highest preemption level
    //max_preemp_block = max_234_tree_search(root_btree);
    count_tmp = root_btree->count; //The count is always pointed to the maximum preemption lvl of the OS_TCB
    max_preemp_block = root_btree->OS_TCB_BLOCK[count_tmp];
  
      //block_list_count = max_preemp_block->count;
      OSTCB_TASK_PREEMP = root_btree->val[count_tmp];//get the maximum preemption lvl of the OS_TCB
      if (OSTCB_TASK_PREEMP > sys_ceiling) { 
      p_tcb         = max_preemp_block;
      max_preemp_block = NULL;     
      deletion(OSTCB_TASK_PREEMP,root_btree);//Delete from the block list
      } else {
              //Assign NULL and release the mutex
                p_mutex->OwnerTCBPtr     = (OS_TCB       *)0;
                p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0;        
                OS_CRITICAL_EXIT();
                *p_err = OS_ERR_NONE;
                return;};    
    
    } 
    else {// else OTHER_MUTEX_BLOCK == 1 , it means there is task pending on different Mutex

    //User added
    //To check and if there is an update to release the OS_TCB_MUTEX_PRE_OWNER(last mutex owner)
    if ((OSTCBCurPtr == mutex_post_ptr->OS_TCB_MUTEX_OWNER) && (OSTCBCurPtr == OS_TCB_MUTEX_PRE_OWNER) && (MUTEX_PRE_OWN == mutex_post_ptr)) {
      OS_TCB_MUTEX_PRE_OWNER = NULL;            //Release OS_TCB_MUTEX_PRE_OWNER to NULL
      mutex_pre_own = 0;                        //Clear mutex_pre_own 
    }

    //User added
    //Assign NULL to OS_TCB_MUTEX_OWNER of the mutex node
    //Assign 0 to task_preemp_lvl
    mutex_post_ptr->OS_TCB_MUTEX_OWNER = NULL;
    mutex_post_ptr->task_preemp_lvl = 0;
 
        //User added
    //Assign NULL to OS_TCB_MUTEX_OWNER of the mutex node
    //Assign 0 to task_preemp_lvl
    //mutex_post_ptr->OS_TCB_MUTEX_OWNER = NULL;
    //mutex_post_ptr->task_preemp_lvl = 0;
      
    //User added
    //->Check for all of the Mutex node, if there is any OSTCBTASK owner or not
    //  -> If yes, find the next highest peremp,and adjust the system ceiling accordingly
    //  -> if Not, no need to find, adjust the system ceiling to the lowest value
    any_task_mutex_owner = 0;
    any_task_mutex_owner = Inorder(splay_root);
    if (any_task_mutex_owner == 1) {//Find the next highest task preemp level and adjust system ceiling base on which mutex
      mutex_post_nxt_high_ptr = splay_preemp_max(splay_preemp_root);
      if(mutex_post_nxt_high_ptr->mutex_id == 1) 
        sys_ceiling = 5;
      else if (mutex_post_nxt_high_ptr->mutex_id == 2) 
        sys_ceiling = 7;
      else if (mutex_post_nxt_high_ptr->mutex_id == 3) 
        sys_ceiling = 7;        
    } else {    //All the mutex are free of OSTCBTASK owner
      sys_ceiling = 4;
    }
    
     //User Added, assign the OSTASKTCB that is in the block list with highest preemption level      
    //Using 234 tree data structure   
    count_tmp = root_btree->count; //The count is always pointed to the maximum preemption lvl of the OS_TCB
    max_preemp_block = root_btree->OS_TCB_BLOCK[count_tmp]; 

      OSTCB_TASK_PREEMP = root_btree->val[count_tmp];//get the maximum preemption lvl of the OS_TCB
      if (OSTCB_TASK_PREEMP > sys_ceiling) { 
      p_tcb         = max_preemp_block;
      p_mutex       = root_btree->MUTEX_BLOCK_PTR[count_tmp];
      max_preemp_block = NULL;
      deletion(OSTCB_TASK_PREEMP,root_btree);//Delete from the block list
      //assign the OS_TCB that gain the mutex into Splay tree node,both id node and preemption node;
      if(*p_mutex->NamePtr == 'a') {
        mutex_post_ptr=Find(1);//Find the node;
        mutex_post_ptr->OS_TCB_MUTEX_OWNER = p_tcb;
        mutex_post_ptr->task_preemp_lvl = OSTCB_TASK_PREEMP;
        //Insert into the preemp Splay tree data structure
        mutex1_preemp_node.OS_TCB_MUTEX_OWNER = p_tcb;
        splay_preemp_Insert(OSTCB_TASK_PREEMP,&mutex1_preemp_node);               
      };
      
      if(*p_mutex->NamePtr == 'b') {
        mutex_post_ptr=Find(2);//Find the node;
        mutex_post_ptr->OS_TCB_MUTEX_OWNER = p_tcb;
        mutex_post_ptr->task_preemp_lvl = OSTCB_TASK_PREEMP;
        //Insert into the preemp Splay tree data structure
        mutex2_preemp_node.OS_TCB_MUTEX_OWNER = p_tcb;
        splay_preemp_Insert(OSTCB_TASK_PREEMP,&mutex2_preemp_node); 
        
      };        
      if(*p_mutex->NamePtr == 'c') {
        mutex_post_ptr=Find(3);//Find the node;
        mutex_post_ptr->OS_TCB_MUTEX_OWNER = p_tcb;
        mutex_post_ptr->task_preemp_lvl = OSTCB_TASK_PREEMP;
        //Insert into the preemp Splay tree data structure
        mutex3_preemp_node.OS_TCB_MUTEX_OWNER = p_tcb;
        splay_preemp_Insert(OSTCB_TASK_PREEMP,&mutex3_preemp_node); 
      };
 
    if (OSTCBCurPtr->Prio != p_mutex->OwnerOriginalPrio) {
        OS_RdyListRemove(OSTCBCurPtr);
        OSTCBCurPtr->Prio = p_mutex->OwnerOriginalPrio;     /* Lower owner's priority back to its original one        */
        OS_PrioInsert(OSTCBCurPtr->Prio);
        OS_RdyListInsertTail(OSTCBCurPtr);                  /* Insert owner in ready list at new priority             */
        OSPrioCur         = OSTCBCurPtr->Prio;
    }
    
      /* Get TCB from head of pend list                         */
    //p_tcb                      = p_pend_list->HeadPtr->TCBPtr;
    //User change
    /* Give mutex to new owner                                */
      if (BENCHMARK_CODE > 0) { 
      //if (MUTEX_PRE_OWN->task_preemp_lvl < TASK_PREEMP) {
        Mutex_OverheadValue_tmp = OS_TS_GET();
        Mutex_OverheadValue = Mutex_StartTime2 + Mutex_OverheadValue_tmp - Mutex_StartTime2 - Mutex_StartTime;
      //}
        Mutex_OverheadValue_tmp = Mutex_OverheadValue;
    }
         
    p_mutex->OwnerTCBPtr       = p_tcb;                     
    p_mutex->OwnerOriginalPrio = p_tcb->Prio;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;   
    
    //User added, update the MUTEX_PREOWNER
    if (mutex_pre_own == 0) {
      OS_TCB_MUTEX_PRE_OWNER = p_tcb; 
      MUTEX_PRE_OWN = mutex_post_ptr;
      mutex_pre_own = 1;
    }
    
     OS_Post((OS_PEND_OBJ *)((void *)p_mutex),
            (OS_TCB      *)p_tcb,
            (void        *)0,
            (OS_MSG_SIZE  )0,
            (CPU_TS       )ts);

    //OS_CRITICAL_EXIT_NO_SCHED();
    
     //if ((opt & OS_OPT_POST_NO_SCHED) == (OS_OPT)0) {
      //  OSSched();                                          /* Run the scheduler                                      */
    //}   
                OS_CRITICAL_EXIT();
                *p_err = OS_ERR_NONE;  
                return;
     
     } else {
                 OS_CRITICAL_EXIT();
                *p_err = OS_ERR_NONE;
                return;};      

    }


                                                      /* Get TCB from head of pend list                         */
    //p_tcb                      = p_pend_list->HeadPtr->TCBPtr;
    //User change
    p_mutex->OwnerTCBPtr       = p_tcb;                     /* Give mutex to new owner                                */
    p_mutex->OwnerOriginalPrio = p_tcb->Prio;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
                                                            /* Post to mutex                                          */
    OS_Post((OS_PEND_OBJ *)((void *)p_mutex),
            (OS_TCB      *)p_tcb,
            (void        *)0,
            (OS_MSG_SIZE  )0,
            (CPU_TS       )ts);

    OS_CRITICAL_EXIT_NO_SCHED();

    if ((opt & OS_OPT_POST_NO_SCHED) == (OS_OPT)0) {
        OSSched();                                          /* Run the scheduler                                      */
    }

    *p_err = OS_ERR_NONE;
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                            CLEAR THE CONTENTS OF A MUTEX
*
* Description: This function is called by OSMutexDel() to clear the contents of a mutex
*

* Argument(s): p_mutex      is a pointer to the mutex to clear
*              -------
*
* Returns    : none
*
* Note(s)    : This function is INTERNAL to uC/OS-III and your application should not call it.
************************************************************************************************************************
*/

void  OS_MutexClr (OS_MUTEX  *p_mutex)
{
    p_mutex->Type              =  OS_OBJ_TYPE_NONE;         /* Mark the data structure as a NONE                      */
    p_mutex->NamePtr           = (CPU_CHAR     *)((void *)"?MUTEX");
    p_mutex->OwnerTCBPtr       = (OS_TCB       *)0;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)0;
    p_mutex->TS                = (CPU_TS        )0;
    p_mutex->OwnerOriginalPrio =  OS_CFG_PRIO_MAX;
    OS_PendListInit(&p_mutex->PendList);                    /* Initialize the waiting list                            */
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                          ADD/REMOVE MUTEX TO/FROM DEBUG LIST
*
* Description: These functions are called by uC/OS-III to add or remove a mutex to/from the debug list.
*
* Arguments  : p_mutex     is a pointer to the mutex to add/remove
*
* Returns    : none
*
* Note(s)    : These functions are INTERNAL to uC/OS-III and your application should not call it.
************************************************************************************************************************
*/


#if OS_CFG_DBG_EN > 0u
void  OS_MutexDbgListAdd (OS_MUTEX  *p_mutex)
{
    p_mutex->DbgNamePtr               = (CPU_CHAR *)((void *)" ");
    p_mutex->DbgPrevPtr               = (OS_MUTEX *)0;
    if (OSMutexDbgListPtr == (OS_MUTEX *)0) {
        p_mutex->DbgNextPtr           = (OS_MUTEX *)0;
    } else {
        p_mutex->DbgNextPtr           =  OSMutexDbgListPtr;
        OSMutexDbgListPtr->DbgPrevPtr =  p_mutex;
    }
    OSMutexDbgListPtr                 =  p_mutex;
}



void  OS_MutexDbgListRemove (OS_MUTEX  *p_mutex)
{
    OS_MUTEX  *p_mutex_next;
    OS_MUTEX  *p_mutex_prev;


    p_mutex_prev = p_mutex->DbgPrevPtr;
    p_mutex_next = p_mutex->DbgNextPtr;

    if (p_mutex_prev == (OS_MUTEX *)0) {
        OSMutexDbgListPtr = p_mutex_next;
        if (p_mutex_next != (OS_MUTEX *)0) {
            p_mutex_next->DbgPrevPtr = (OS_MUTEX *)0;
        }
        p_mutex->DbgNextPtr = (OS_MUTEX *)0;

    } else if (p_mutex_next == (OS_MUTEX *)0) {
        p_mutex_prev->DbgNextPtr = (OS_MUTEX *)0;
        p_mutex->DbgPrevPtr      = (OS_MUTEX *)0;

    } else {
        p_mutex_prev->DbgNextPtr =  p_mutex_next;
        p_mutex_next->DbgPrevPtr =  p_mutex_prev;
        p_mutex->DbgNextPtr      = (OS_MUTEX *)0;
        p_mutex->DbgPrevPtr      = (OS_MUTEX *)0;
    }
}
#endif

/*$PAGE*/
/*
************************************************************************************************************************
*                                                MUTEX INITIALIZATION
*
* Description: This function is called by OSInit() to initialize the mutex management.
*

* Argument(s): p_err        is a pointer to a variable that will contain an error code returned by this function.
*
*                                OS_ERR_NONE     the call was successful
*
* Returns    : none
*
* Note(s)    : 1) This function is INTERNAL to uC/OS-III and your application MUST NOT call it.
************************************************************************************************************************
*/

void  OS_MutexInit (OS_ERR  *p_err)
{
#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_DBG_EN > 0u
    OSMutexDbgListPtr = (OS_MUTEX *)0;
#endif

    OSMutexQty        = (OS_OBJ_QTY)0;
    *p_err            =  OS_ERR_NONE;
}


/*
************************************************************************************************************************
*                                               USER Added
*                                               Function for Splay tree for Mutex/Preemp lvl searching Splay tree
*
************************************************************************************************************************
*/

void rightRotate(struct  mutex_node *P)
{
	//struct  mutex_node *T=P->l;
	//struct  mutex_node *B=T->r;
	//struct  mutex_node *D=P->p;
	struct  mutex_node *T;
	struct  mutex_node *B;
	struct  mutex_node *D;
        
        
        if(P->l == NULL)
          T=NULL;
        else
          T=P->l;
        
        if(T == NULL)
          B = NULL;
        else
          B=T->r;
        
        if(P->p==NULL)
          D=NULL;
        else
          D=P->p;
        
        
	if(D)
	{
		if(D->r==P) D->r=T;
		else D->l=T;
	}
	if(B)
		B->p=P;
	T->p=D;
	T->r=P;
	
	P->p=T;
	P->l=B;
        
        return;
}
void leftRotate(struct mutex_node *P)
{
	//struct  mutex_node *T=P->r;
	//struct  mutex_node *B=T->l;
	//struct  mutex_node *D=P->p;
	struct  mutex_node* T;
	struct  mutex_node* B;
	struct  mutex_node* D;     
        
        if(P->r == NULL)
          T=NULL;
        else
          T= P->r;
        
        if(T == NULL)
          B=NULL;
        else
          B=T->l;
        
        if(P->p == NULL)
          D = NULL;
        else
          D=P->p;
        
        
	if(D)
	{
		if(D->r==P) D->r=T;
		else D->l=T;
	}
	if(B)
		B->p=P;
	T->p=D;
	T->l=P;
	
	P->p=T;
	P->r=B;
        
        return;
}

void Splay(struct mutex_node *T)
{
	while(1)
	{
		struct  mutex_node *p=T->p;
		if(!p) break;
		struct mutex_node *pp=p->p;
		if(!pp)//Zig
		{
			if(p->l==T)
				rightRotate(p);
			else
				leftRotate(p);
			break;
		}
		if(pp->l==p)
		{
			if(p->l==T)
			{//ZigZig
				rightRotate(pp);
				rightRotate(p);
			}
			else
			{//ZigZag
				leftRotate(p);
				rightRotate(pp);
			}
		}
		else
		{
			if(p->l==T)
			{//ZigZag
				rightRotate(p);
				leftRotate(pp);
			}
			else
			{//ZigZig
				leftRotate(pp);
				leftRotate(p);
			}
		}
	}
        
        //if(preemp_node == 0) //If it is a not a  splay preemp node
          splay_root=T;
        //else
        //  splay_preemp_root=T;
        
        return;
}


void Splay_preemp(struct mutex_node *T)
{
	while(1)
	{
		struct  mutex_node *p=T->p;
		if(!p) break;
		struct mutex_node *pp=p->p;
		if(!pp)//Zig
		{
			if(p->l==T)
				rightRotate(p);
			else
				leftRotate(p);
			break;
		}
		if(pp->l==p)
		{
			if(p->l==T)
			{//ZigZig
				rightRotate(pp);
				rightRotate(p);
			}
			else
			{//ZigZag
				leftRotate(p);
				rightRotate(pp);
			}
		}
		else
		{
			if(p->l==T)
			{//ZigZag
				rightRotate(p);
				leftRotate(pp);
			}
			else
			{//ZigZig
				leftRotate(pp);
				leftRotate(p);
			}
		}
	}
	splay_preemp_root=T;
        
        return;
}

                     
void splay_Insert(int v, struct mutex_node* mutex123_node)
{
        
	if(splay_root->v == 0)
	{
                splay_root = mutex123_node;             
		return;
	}
        //P is point to the splay_root node
	struct mutex_node *P;
        
        P     = splay_root;
        
	while(true)
	{
		//if(P->v==v) break; // not multiset
		if(v < (P->v) )
		{
			if(P->l) P=P->l; //if the Left node of the splay root_btree is not a NULL
			else
			{
				//P->l=(struct mutex_node *)malloc(sizeof(struct mutex_node));
                                //P->l is = mutex123_node
				//P->l->p=P;
				//P->l->r=NULL;
				//P->l->l=NULL;
				//P->l->v=v;
				//P=P->l;
                                //P->l is = mutex123_node
                                P->l = mutex123_node;
				mutex123_node->p=P;
				mutex123_node->r=NULL;
				mutex123_node->l=NULL;
				mutex123_node->v=v;
                                mutex123_node->mutex_id=v;
                                //mutex123_node->OS_TCB_MUTEX_OWNER = OSTCBCurPtr;
				P=mutex123_node;
				break;
			}
		}
		else
		{
			if(P->r) P=P->r;
			else
			{
				//P->r=(struct mutex_node *)malloc(sizeof(struct mutex_node));
				//P->r->p=P;
				//P->r->r=NULL;
				//P->r->l=NULL;
				//P->r->v=v;
				//P=P->r;
                          
                                //P->r is = mutex123_node
                                P->r = mutex123_node;
				mutex123_node->p=P;
				mutex123_node->r=NULL;
				mutex123_node->l=NULL;
				mutex123_node->v=v;
                                mutex123_node->mutex_id=v;
                                //mutex123_node->OS_TCB_MUTEX_OWNER = OSTCBCurPtr;
				P=mutex123_node;
				break;
			}
		}
	}
	Splay(P);
        
        return;
}

void splay_preemp_Insert(int v, struct mutex_node* mutex123_node)
{
        
	if(splay_preemp_root->v == 0 || splay_preemp_root == NULL)
	{
		mutex123_node->p=NULL;
		mutex123_node->r=NULL;
		mutex123_node->l=NULL;
		mutex123_node->v=v;
                mutex123_node->task_preemp_lvl=v;
                splay_preemp_root = mutex123_node;  
		return;
	}
        //P is point to the splay_root node
	struct mutex_node *P;
        
        P     = splay_preemp_root;
        
	while(true)
	{
          if(P->v==v) {//If the preemption level is the same, change the root_btree node to the latest node                        
				mutex123_node->p=NULL;
				mutex123_node->r=NULL;
				mutex123_node->l=P;
				mutex123_node->v=v;
                                mutex123_node->task_preemp_lvl=v;
                                splay_preemp_root->p= mutex123_node;
				splay_preemp_root=mutex123_node; 
                                return;
          };
          
		if(v < (P->v) )
		{
			if(P->l) P=P->l; //if the Left node of the splay root_btree is not a NULL
			else
			{
				//P->l=(struct mutex_node *)malloc(sizeof(struct mutex_node));
                                //P->l is = mutex123_node
				//P->l->p=P;
				//P->l->r=NULL;
				//P->l->l=NULL;
				//P->l->v=v;
				//P=P->l;
                                //P->l is = mutex123_node
                                P->l = mutex123_node;
				mutex123_node->p=P;
				mutex123_node->r=NULL;
				mutex123_node->l=NULL;
				mutex123_node->v=v;
                                mutex123_node->task_preemp_lvl=v;
				P=mutex123_node;
				break;
			}
		}
		else
		{
			if(P->r) P=P->r;
			else
			{
				//P->r=(struct mutex_node *)malloc(sizeof(struct mutex_node));
				//P->r->p=P;
				//P->r->r=NULL;
				//P->r->l=NULL;
				//P->r->v=v;
				//P=P->r;
                          
                                //P->r is = mutex123_node
                                P->r = mutex123_node;
				mutex123_node->p=P;
				mutex123_node->r=NULL;
				mutex123_node->l=NULL;
				mutex123_node->v=v;
                                mutex123_node->task_preemp_lvl=v;
				P=mutex123_node;
				break;
			}
		}
	}
	Splay_preemp(P);
        
        return;
}

int Inorder(struct mutex_node *R)
{
	if(!R) return 0;
	Inorder(R->l);
	//printf("v: %d ",R->v);
	if(R->l != NULL && R->l->OS_TCB_MUTEX_OWNER != NULL) return 1;//There is still OSTCBTASK owner for the MUTEX
	if(R->r != NULL && R->r->OS_TCB_MUTEX_OWNER != NULL) return 1;//There is still OSTCBTASK owner for the MUTEX
	//puts("");
	Inorder(R->r);
        
        return 0;
}
struct mutex_node* Find(int v)
{
        //struct mutex_node *P;
        //if (preemp_node == 0) { //check if it is not a preemp node
	if(!splay_root) return NULL;
	struct mutex_node *P=splay_root;
        //} else {//it is a preemp node
	//if(!splay_preemp_root) return NULL;
	//P=splay_preemp_root;                
        //}
	while(P)
	{
		if(P->v==v)
			break;
		if(v<(P->v))
		{
			if(P->l)
				P=P->l;
			else
				break;
		}
		else
		{
			if(P->r)
				P=P->r;
			else
				break;
		}
	}
        //if (preemp_node == 0)
          Splay(P);
        //else 
          //Splay(P,1);
        
	if(P->v==v) return P;
	else return NULL;
}


struct mutex_node* Find_preemp(int v)
{
	if(!splay_preemp_root) return NULL;
	struct mutex_node *P=splay_preemp_root;
	while(P)
	{
		if(P->v==v)
			break;
		if(v<(P->v))
		{
			if(P->l)
				P=P->l;
			else
				break;
		}
		else
		{
			if(P->r)
				P=P->r;
			else
				break;
		}
	}
	Splay_preemp(P);
	if(P->v==v) return P;
	else return NULL;
}

/*
int Erase(int v)
{
	struct mutex_node *N=Find(v);
	if(!N) return false;
	Splay(N); //check once more;
	struct mutex_node *P=N->l;
	if(!P)
	{
		splay_root=N->r;
		splay_root->p=NULL;
		free(N);
		return true;
	}
	while(P->r) P=P->r;
	if(N->r)
	{
		P->r=N->r;
		N->r->p=P;
	}
	splay_root=N->l;
	splay_root->p=NULL;
	free(N);
	return 1;
}
*/

int Erase_preemp(int v,int mutex_id)
{
	struct mutex_node *N=Find_preemp(v);
        //Keep searching left node if it does not match
        while(N->mutex_id != mutex_id) {N = N->l;};
        
        //struct mutex_node *N=Find(v,1);
	if(!N) return false;
	Splay_preemp(N); //check once more;
        //Splay(N,1); //check once more;
	struct mutex_node *P=N->l;
	if(!P)
	{
		splay_preemp_root=N->r;
		splay_preemp_root->p=NULL;
		//free(N);
		return true;
	}
	while(P->r) P=P->r;
	if(N->r)
	{
		P->r=N->r;
		N->r->p=P;
	}
	splay_preemp_root=N->l;
	splay_preemp_root->p=NULL;
	//free(N);
	return 1;
}

struct mutex_node* splay_preemp_max(struct mutex_node* node) {
    while (node->r != NULL) node = node->r;
    return node;
}

/*********************************************************************************************************
          
                              Block List Management(2-3-4 tree data structure)

*********************************************************************************************************/  

  /* creating new node */
  struct btreeNode * createNode(int val,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR,struct btreeNode* mutex_node_block, struct btreeNode *child) {
        //struct btreeNode *newNode;
        //newNode = (struct btreeNode *)malloc(sizeof(struct btreeNode));
        //newNode->val[1] = val;
        //newNode->count = 1;
        //newNode->link[0] = root_btree;
        //newNode->link[1] = child;
        //return newNode;
    
        mutex_node_block->val[1] = val; //preemption lvl
        mutex_node_block->OS_TCB_BLOCK[1] = OSTCBCurPtr;
        mutex_node_block->MUTEX_BLOCK_PTR[1] = MUTEX_BLOCK_PTR;
        mutex_node_block->count = 1;
        mutex_node_block->link[0] = root_btree;
        mutex_node_block->link[1] = child;
       
        return mutex_node_block;
  }

  /* Places the value in appropriate position */
  void addValToNode(int val, int pos, struct btreeNode *node,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR,struct btreeNode *child) {
        int j = node->count;
        while (j > pos) {
                node->val[j + 1] = node->val[j];
                node->OS_TCB_BLOCK[j + 1] = node->OS_TCB_BLOCK[j];
                node->MUTEX_BLOCK_PTR[j + 1] = node->MUTEX_BLOCK_PTR[j];
                node->link[j + 1] = node->link[j];
                j--;
        }
        node->val[j + 1] = val;
        node->OS_TCB_BLOCK[j + 1] = OSTCBCurPtr;
        node->MUTEX_BLOCK_PTR[j + 1] = MUTEX_BLOCK_PTR;
        node->link[j + 1] = child;
        node->count++;
  }

  /* split the node */
  void splitNode (int val, int *pval, int pos, struct btreeNode *node,
     struct btreeNode *child,OS_TCB* OSTCBCurPtr,struct btreeNode **newNode) {
        int median, j;
        OS_MUTEX* MUTEX_BLOCK_PTR;
        if (pos > MIN)
                median = MIN + 1;
        else
                median = MIN;

        *newNode = (struct btreeNode *)malloc(sizeof(struct btreeNode));
        j = median + 1;
        while (j <= MAX) {
                (*newNode)->val[j - median] = node->val[j];
                (*newNode)->OS_TCB_BLOCK[j - median] = node->OS_TCB_BLOCK[j];
                (*newNode)->link[j - median] = node->link[j];
                j++;
        }
        node->count = median;
        (*newNode)->count = MAX - median;

        if (pos <= MIN) {
                addValToNode(val, pos, node,OSTCBCurPtr ,MUTEX_BLOCK_PTR,child);
        } else {
                addValToNode(val, pos - median, *newNode, OSTCBCurPtr,MUTEX_BLOCK_PTR,child);
        }
        *pval = node->val[node->count];
        (*newNode)->link[0] = node->link[node->count];
        node->count--;
  }

  /* sets the value val in the node */
  int setValueInNode(int val, int *pval,struct btreeNode *node,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR,struct btreeNode **child) {

        int pos;
        if (!node) {
                *pval = val;
                *child = NULL;
                return 1;
        }

        if (val < node->val[1]) {
                pos = 0;
        } else {
                for (pos = node->count;
                        (val < node->val[pos] && pos > 1); pos--);
                if (val == node->val[pos]) {
                        printf("Duplicates not allowed\n");
                        return 0;
                }
        }
        if (setValueInNode(val, pval, node->link[pos],OSTCBCurPtr,MUTEX_BLOCK_PTR,child)) {
                if (node->count < MAX) {
                        addValToNode(*pval, pos, node,OSTCBCurPtr,MUTEX_BLOCK_PTR ,*child);
                } else {
                        splitNode(*pval, pval, pos, node, *child,OSTCBCurPtr ,child);
                        return 1;
                }
        }
        return 0;
  }

  /* insert val in B-Tree */
  void insertion(int val,OS_TCB* OSTCBCurPtr,OS_MUTEX  *MUTEX_BLOCK_PTR,struct btreeNode* mutex_node_block) {
        int flag, i;
        struct btreeNode *child;

        flag = setValueInNode(val, &i, root_btree,OSTCBCurPtr,MUTEX_BLOCK_PTR,&child);
        if (flag)
                root_btree = createNode(i,OSTCBCurPtr,MUTEX_BLOCK_PTR,mutex_node_block,child);
  }

  /* copy successor for the value to be deleted */
  void copySuccessor(struct btreeNode *myNode, int pos) {
        struct btreeNode *dummy;
        dummy = myNode->link[pos];

        for (;dummy->link[0] != NULL;)
                dummy = dummy->link[0];
        myNode->val[pos] = dummy->val[1];

  }

  /* removes the value from the given node and rearrange values */
  void removeVal(struct btreeNode *myNode, int pos) {
        int i = pos + 1;
        while (i <= myNode->count) {
                myNode->val[i - 1] = myNode->val[i];
                myNode->link[i - 1] = myNode->link[i];
                i++;
        }
        myNode->count--;
  }

  /* shifts value from parent to right child */
  void doRightShift(struct btreeNode *myNode, int pos) {
        struct btreeNode *x = myNode->link[pos];
        int j = x->count;

        while (j > 0) {
                x->val[j + 1] = x->val[j];
                x->link[j + 1] = x->link[j];
        }
        x->val[1] = myNode->val[pos];
        x->link[1] = x->link[0];
        x->count++;

        x = myNode->link[pos - 1];
        myNode->val[pos] = x->val[x->count];
        myNode->link[pos] = x->link[x->count];
        x->count--;
        return;
  }

  /* shifts value from parent to left child */
  void doLeftShift(struct btreeNode *myNode, int pos) {
        int j = 1;
        struct btreeNode *x = myNode->link[pos - 1];

        x->count++;
        x->val[x->count] = myNode->val[pos];
        x->link[x->count] = myNode->link[pos]->link[0];

        x = myNode->link[pos];
        myNode->val[pos] = x->val[1];
        x->link[0] = x->link[1];
        x->count--;

        while (j <= x->count) {
                x->val[j] = x->val[j + 1];
                x->link[j] = x->link[j + 1];
                j++;
        }
        return;
  }

  /* merge nodes */
  void mergeNodes(struct btreeNode *myNode, int pos) {
        int j = 1;
        struct btreeNode *x1 = myNode->link[pos], *x2 = myNode->link[pos - 1];

        x2->count++;
        x2->val[x2->count] = myNode->val[pos];
        x2->link[x2->count] = myNode->link[0];

        while (j <= x1->count) {
                x2->count++;
                x2->val[x2->count] = x1->val[j];
                x2->link[x2->count] = x1->link[j];
                j++;
        }

        j = pos;
        while (j < myNode->count) {
                myNode->val[j] = myNode->val[j + 1];
                myNode->link[j] = myNode->link[j + 1];
                j++;
        }
        myNode->count--;
        //free(x1);
  }

  /* adjusts the given node */
  void adjustNode(struct btreeNode *myNode, int pos) {
        if (!pos) {
                if (myNode->link[1]->count > MIN) {
                        doLeftShift(myNode, 1);
                } else {
                        mergeNodes(myNode, 1);
                }
        } else {
                if (myNode->count != pos) {
                        if(myNode->link[pos - 1]->count > MIN) {
                                doRightShift(myNode, pos);
                        } else {
                                if (myNode->link[pos + 1]->count > MIN) {
                                        doLeftShift(myNode, pos + 1);
                                } else {
                                        mergeNodes(myNode, pos);
                                }
                        }
                } else {
                        if (myNode->link[pos - 1]->count > MIN)
                                doRightShift(myNode, pos);
                        else
                                mergeNodes(myNode, pos);
                }
        }
  }

  /* delete val from the node */
  int delValFromNode(int val, struct btreeNode *myNode) {
        int pos, flag = 0;
        if (myNode) {
                if (val < myNode->val[1]) {
                        pos = 0;
                        flag = 0;
                } else {
                        for (pos = myNode->count;
                                (val < myNode->val[pos] && pos > 1); pos--);
                         if (val == myNode->val[pos]) {
                                flag = 1;
                        } else {
                                flag = 0;
                        }
                }
                if (flag) {
                        if (myNode->link[pos - 1]) {
                                copySuccessor(myNode, pos);
                                flag = delValFromNode(myNode->val[pos], myNode->link[pos]);
                                if (flag == 0) {
                                        printf("Given data is not present in B-Tree\n");
                                }
                        } else {
                                removeVal(myNode, pos);
                        }
                } else {
                        flag = delValFromNode(val, myNode->link[pos]);
                }
                if (myNode->link[pos]) {
                        if (myNode->link[pos]->count < MIN)
                                adjustNode(myNode, pos);
                }
        }
        return flag;
  }

  /* delete val from B-tree */
  void deletion(int val, struct btreeNode *myNode) {
        struct btreeNode *tmp;
        if (!delValFromNode(val, myNode)) {
                printf("Given value is not present in B-Tree\n");
                return;
        } else {
                if (myNode->count == 0) {
                        tmp = myNode;
                        //Assign NULL
                        
                        myNode = myNode->link[0];
                        
                        //free(tmp);
                }
        }
        root_btree = myNode;
        return;
  }
        

  /* search val in B-Tree */
struct btreeNode* searching(int val, int pos, struct btreeNode *myNode) {
        if (!myNode) {
                return NULL;
        }

        if (val < myNode->val[1]) {
                pos = 0;
        } else {
                for (pos = myNode->count;
                        (val < myNode->val[pos] && pos > 1); (pos)--);
                if (val == myNode->val[pos]) {
                        printf("Given data %d is present in B-Tree", val);
                        return myNode;
                }
        }
        searching(val, pos, myNode->link[pos]);
        return myNode;
  }

//struct btreeNode* max_234_tree_search(struct btreeNode* node) {
//    while (node->r != NULL) node = node->r;
//    return node;
//}

  /* B-Tree Traversal */
/*
  void traversal(struct btreeNode *myNode) {
        int i;
        if (myNode) {
                for (i = 0; i < myNode->count; i++) {
                        traversal(myNode->link[i]);
                        printf("%d ", myNode->val[i + 1]);
                }
                traversal(myNode->link[i]);
        }
  }
*/
#endif                                                      /* OS_CFG_MUTEX_EN                                        */
